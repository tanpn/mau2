<!DOCTYPE html>
<!--[if IE]><![endif]-->
<!--[if IE 8 ]><html dir="ltr" lang="en" class="ie8"><![endif]-->
<!--[if IE 9 ]><html dir="ltr" lang="en" class="ie9"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html dir="ltr" lang="en" class="">
<!--<![endif]-->
<head>
	<meta charset="UTF-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Cosmetorix</title>
	<base href="" />
		<meta name="description" content="Cosmetorix" />
				<!-- Fonts -->	
	<script src="/cdn-cgi/apps/head/3ts2ksMwXvKRuG480KNifJ2_JNM.js"></script><link href='//fonts.googleapis.com/css?family=Arimo&subset=latin,cyrillic' rel='stylesheet' type='text/css'>
	<link href='//fonts.googleapis.com/css?family=Raleway:400,700' rel='stylesheet' type='text/css'>
	<link href="catalog/view/javascript/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
	<!-- END Fonts -->
	<link href="catalog/view/javascript/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen" />

	
	<link href="catalog/view/theme/theme694/js/owl.carousel/assets/owl.carousel.css" rel="stylesheet">
	<link href="catalog/view/theme/theme694/stylesheet/material-design.css" rel="stylesheet">	
	<link href="catalog/view/theme/theme694/stylesheet/fl-outicons.css" rel="stylesheet">	
	<link href="catalog/view/theme/theme694/stylesheet/material-icons.css" rel="stylesheet">	
	<link href="catalog/view/theme/theme694/js/fancybox/jquery.fancybox.css" rel="stylesheet">
	<link href="catalog/view/theme/theme694/stylesheet/photoswipe.css" rel="stylesheet">
	<link href="catalog/view/theme/theme694/stylesheet/stylesheet.css" rel="stylesheet">

	
		<link id="color_scheme" href="catalog/view/theme/theme694/stylesheet/color_schemes/color_scheme_4.css" rel="stylesheet">
	
		<link href="catalog/view/javascript/jquery/owl-carousel/owl.carousel.css" type="text/css" rel="stylesheet" media="screen" />
		<link href="catalog/view/javascript/jquery/owl-carousel/owl.transitions.css" type="text/css" rel="stylesheet" media="screen" />
			<link href="https://livedemo00.template-help.com/opencart_59086/" rel="canonical" />
		<link href="image/catalog/favicon.png" rel="icon" />
	
	<script src="catalog/view/javascript/jquery/jquery-2.1.1.min.js" type="text/javascript"></script>
	<script src="catalog/view/javascript/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>

		<script src="catalog/view/javascript/jquery/owl-carousel/owl.carousel.min.js" type="text/javascript"></script>
		<script src="catalog/view/theme/theme694/js/tmmegamenu/superfish.min.js" type="text/javascript"></script>
		<script src="catalog/view/theme/theme694/js/tmmegamenu/jquery.rd-navbar.min.js" type="text/javascript"></script>
		<script src="catalog/view/theme/theme694/js/bootstrap/bootstrap-tabcollapse.js" type="text/javascript"></script>
		<script src="catalog/view/theme/theme694/js/tmcolorswitcher/jquery.cookies.js" type="text/javascript"></script>
		<script src="catalog/view/theme/theme694/js/tmcolorswitcher/style_switcher_demo.js" type="text/javascript"></script>
		</head>
<body class="common-home">
	<p id="gl_path" class="hidden">theme694</p>
	<div id="page">
		<div class="ie-warning">
			<a href="https://windows.microsoft.com/en-us/internet-explorer/download-ie">
				<img src="catalog/view/theme/theme694/image/warning_bar_0000_us.jpg" border="0" height="75" width="1170" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today."/>
			</a>
		</div>
		<header>
		
			<div class="header-top">
			<div class="container">
				
				<nav id="top-links" class="nav toggle-wrap">
					<a class="toggle fl-outicons-user189" href='#'></a>
					<ul class="toggle_cont">
						<li> <a href="https://livedemo00.template-help.com/opencart_59086/index.php?route=checkout/checkout" title="Checkout"> Checkout </a> </li>
												<li><a href="https://livedemo00.template-help.com/opencart_59086/index.php?route=account/login">Login</a></li>
						<li><a href="https://livedemo00.template-help.com/opencart_59086/index.php?route=account/register">Register</a></li>
												<li><a href="https://livedemo00.template-help.com/opencart_59086/index.php?route=information/contact">Contact</a></li>
					</ul>
				</nav>
				
				<div class="box-currency">
	<form action="https://livedemo00.template-help.com/opencart_59086/index.php?route=common/currency/currency" method="post" enctype="multipart/form-data" id="form-currency">
		<div class="btn-group toggle-wrap">
			<span class="toggle">
												<span>USD</span>
																												<span class="hidden-xs">Currency</span>
			</span>
			<ul class="toggle_cont pull-right">
																<li>
					<button class="currency-select selected" type="button" name="USD">
						USD						Dollar					</button>
				</li>
																								<li>
					<button class="currency-select" type="button" name="EUR">
						EUR						Euro					</button>
				</li>
																								<li>
					<button class="currency-select" type="button" name="GBP">
						GBP						Pound					</button>
				</li>
															</ul>
		</div>
		<input type="hidden" name="code" value="" />
		<input type="hidden" name="redirect" value="https://livedemo00.template-help.com/opencart_59086/index.php?route=common/home" />
	</form>
</div>
				<div class="box-language">
	<form action="https://livedemo00.template-help.com/opencart_59086/index.php?route=common/language/language" method="post" enctype="multipart/form-data" id="form-language">
		<div class="btn-group toggle-wrap">
			<span class="toggle">
												
				<span>English</span>
																												<span class="hidden-xs">Language</span>
			</span>
			<ul class="toggle_cont pull-right">
												<li>
					<button class="language-select selected" type="button" name="en-gb">
						English					</button>
				</li>
																<li>
					<button class="language-select" type="button" name="de-DE">
						Deutsch					</button>
				</li>
																<li>
					<button class="language-select" type="button" name="ru-ru">
						Русский					</button>
				</li>
											</ul>
		</div>
		<input type="hidden" name="code" value="" />
		<input type="hidden" name="redirect" value="https://livedemo00.template-help.com/opencart_59086/index.php?route=common/home" />
	</form>
</div>
				
			</div></div>
			
			<div class="logo-block">
				<div class="container">
					<h1 id="logo" class="logo">
												<a href="https://livedemo00.template-help.com/opencart_59086/index.php?route=common/home"><img src="image/catalog/logo.png" title="Cosmetorix" alt="Cosmetorix" class="img-responsive"></a>
											</h1>
					
					<div class="pull-left">
						Call us: <a href="callto:+3(800)2345-6789">+3(800)2345-6789</a>
						<p>7 Days a week from 9:00 am to 7:00 pm</p>
					</div>
					
					<div class="box-cart">
	<div id="cart" class="cart toggle-wrap">
		<button type="button" data-loading-text="Loading..." class="toggle">
			<i class="fl-outicons-shopping-cart13"></i>
			<strong>Shopping Cart</strong>
			<span id="cart-total" class="cart-total">0 item(s) - $0.00</span>
						<span id="cart-total2" class="cart-total2">0</span>
					</button>
		<ul class="pull-right toggle_cont">
			<li>
				Shopping Cart				
				<button type="button" class="btn-close-cart btn-danger"><i class="material-icons-clear"></i></button>
			</li>
						<li>
				<p class="text-center">Your shopping cart is empty!</p>
			</li>
					</ul>
	</div>
</div>					<a class="button-wishlist" href="https://livedemo00.template-help.com/opencart_59086/index.php?route=account/wishlist" id="wishlist-total" title=""> <i class="fl-outicons-heart373"></i> Wishlist <span>0</span> </a>
					
				</div>
			</div>
			<div class="container"><div>
									<div class="stuck-menu navigation"><!-- id="stuck" -->
						 <div class="megamenu">
	<h3>Categories</h3>
	<ul class="sf-menu">
				<li class="sf-with-mega">
						<a href="https://livedemo00.template-help.com/opencart_59086/index.php?route=product/category&amp;path=18">Face</a>
									<ul class="sf-mega" >
								<li class="sf-mega_row">
										<div class="sf-mega_section 1" style="width: 16.7%">
						<h4>Categories</h4>																								<ul>
							<li>
<a href="https://livedemo00.template-help.com/opencart_59086/index.php?route=product/category&amp;path=40">Face</a>
</li>
<li>
<a href="https://livedemo00.template-help.com/opencart_59086/index.php?route=product/category&amp;path=41">Eyes</a>
</li>
<li>
<a href="https://livedemo00.template-help.com/opencart_59086/index.php?route=product/category&amp;path=42">Lips</a>
</li>
<li>
<a href="https://livedemo00.template-help.com/opencart_59086/index.php?route=product/category&amp;path=43">Nails</a>
</li>
<li>
<a href="https://livedemo00.template-help.com/opencart_59086/index.php?route=product/category&amp;path=44">Make Up Brushes</a>
</li>
<li>
<a href="https://livedemo00.template-help.com/opencart_59086/index.php?route=product/category&amp;path=37">Tools &amp; Accessories</a>
</li>
<li>
<a href="https://livedemo00.template-help.com/opencart_59086/index.php?route=product/category&amp;path=39">Make Up Gifts</a>
</li>
						</ul>
											</div>
														<div class="sf-mega_section 4" style="width: 16.8%">
																														<ul>
							<li>
<a href="https://livedemo00.template-help.com/opencart_59086/index.php?route=product/product&amp;product_id=50">Youngman 15 Colors Professional Concealer</a>
</li>
<li>
<a href="https://livedemo00.template-help.com/opencart_59086/index.php?route=product/product&amp;product_id=49">VERAttiva Face Tonic Rinse and Refresher 7-Ounce</a>
</li>
<li>
<a href="https://livedemo00.template-help.com/opencart_59086/index.php?route=product/product&amp;product_id=48">Eye and Lip Primer Base  Paraben Talc Free</a>
</li>
<li>
<a href="https://livedemo00.template-help.com/opencart_59086/index.php?route=product/product&amp;product_id=47">Physicians Formula Youthful Wear Cosmeceutical</a>
</li>
<li>
<a href="https://livedemo00.template-help.com/opencart_59086/index.php?route=product/product&amp;product_id=46">Eye Pencil Ultra Black</a>
</li>
<li>
<a href="https://livedemo00.template-help.com/opencart_59086/index.php?route=product/product&amp;product_id=43">Anti-Aging Daily Moisturizer</a>
</li>
<li>
<a href="https://livedemo00.template-help.com/opencart_59086/index.php?route=product/product&amp;product_id=41">All Day UV Moisture Cream SPF 15</a>
</li>
<li>
<a href="https://livedemo00.template-help.com/opencart_59086/index.php?route=product/product&amp;product_id=40">Olay CC Cream Total Effects Tone Correcting Moisturizer</a>
</li>
<li>
<a href="https://livedemo00.template-help.com/opencart_59086/index.php?route=product/product&amp;product_id=36">NIVEA Body Creme 13_5 Ounce</a>
</li>
<li>
<a href="https://livedemo00.template-help.com/opencart_59086/index.php?route=product/product&amp;product_id=35">RevitaLift Face and Neck Day Cream</a>
</li>
						</ul>
											</div>
														<div class="sf-mega_section 0" style="width: 16.8%">
																		<div class="megamenu_module">
							<div class="box_html More ways to shop">
		<h2 class="title">More ways to shop</h2>
		<ul class="list-unstyled">
<li><a href="index.php?route=simple_blog/article/view&amp;simple_blog_article_id=9">New Arrivals</a></li>
<li><a href="index.php?route=simple_blog/article/view&amp;simple_blog_article_id=10">Cosmetics Under £3</a></li>
<li><a href="index.php?route=account/voucher">Get The Party Look</a></li>
</ul></div>
						</div>
																							</div>
														<div class="sf-mega_section 0" style="width: 16.8%">
																		<div class="megamenu_module">
							<script>
				$(document).ready(function ($) {
				$('#single-category0 .tab-content > .tab-pane').css({
					'display': 'block',
					'visibility': 'visible'
				});
			});
					</script>

		<div class="box single-category">
			<div class="box-heading">
				<h2>Face</h2>
			</div>
			<div class="box-content">
				<div role="tabpanel" class="module_tab" id="single-category0">
															<h3>Featured</h3>
																									
					<!-- Tab panes -->
					<div class="tab-content">
												<div role="tabpanel" class="tab-pane" id="tab-single-featured-0">
							<div class="box clearfix">
								<div class="row">
																											<div class="product-layout col-lg-3 col-md-3 col-sm-3 col-xs-12">
																				<div class="product-thumb transition options">
																						<!-- Product options -->
											<div class="product-option-wrap">
												<div class="product-options form-horizontal">
													<div class="options">
														<a class="ajax-overlay_close" href='#'></a>
														<h3>Available Options</h3>
														<div class="form-group hidden">
															<div class="col-sm-8">
																<input type="text" name="product_id" value="31" class="form-control"/>
															</div>
														</div>
																																										<div class="form-group required">
															<label class="control-label col-sm-12" for="input-option705001">
																Size															</label>
															<div class="col-sm-12">
																<select name="option[7]" id="input-option705001" class="form-control">
																	<option value="">--- Please Select ---</option>
																																		<option value="17">
																		Small																																				(+$20.00)
																																			</option>
																																		<option value="18">
																		Medium																																				(+$15.00)
																																			</option>
																																		<option value="19">
																		Large																																				(+$10.00)
																																			</option>
																																	</select>
															</div>
														</div>
																																																																																																																																																																																																				<div class="form-group required">
															<label class="control-label col-sm-12">Color</label>
															<div class="col-sm-12">
																<div id="input-option805001">
																																		<div class="radio">
																		<label for="option[82005001]">
																			<input type="radio" hidden name="option[8]" id="option[82005001]" value="20"/>
																			red																																						(+$10.00)
																																					</label>
																	</div>
																																	</div>
															</div>
														</div>
																																																																																																																																																										<button class="product-btn-add" type="button" onclick="cart.addPopup($(this),'31');">
															<i class="material-design-shopping231"></i>
															<span>Add to Cart</span>
														</button>
													</div>
												</div>
											</div>
																						<div class="quick_info">
												<div id="quickview_50010" class="quickview-style">
													<div>
														<div class="left col-sm-4">
															<div class="quickview_image image">
																<a href="https://livedemo00.template-help.com/opencart_59086/index.php?route=product/product&amp;product_id=31"><img width="5" height="5" alt="butter LONDON LIPPY Moisture Matte Lipstick" title="butter LONDON LIPPY Moisture Matte Lipstick" class="img-responsive" src="image/cache/catalog/products/product_10-5x5.png"/></a>
															</div>
														</div>
														<div class="right col-sm-8">
															<h2>butter LONDON LIPPY Moisture Matte Lipstick</h2>
															<div class="inf">
																																<p class="quickview_manufacture manufacture manufacture">Brand:																	<a href="https://livedemo00.template-help.com/opencart_59086/index.php?route=product/manufacturer/info&amp;manufacturer_id=28">Olay</a>
																</p>
																																																<p class="product_model model">Model: Collectible</p>
																																																<div class="price">
																																		$70.00																																																		</div>
																															</div>
																														<div class="rating">
																																																<span class="fa-stack">
																	<i class="material-design-bookmark45 fa-stack-1x"></i>
																	<i class="material-design-bookmark45 star fa-stack-1x"></i>
																</span>
																																																																<span class="fa-stack">
																	<i class="material-design-bookmark45 fa-stack-1x"></i>
																	<i class="material-design-bookmark45 star fa-stack-1x"></i>
																</span>
																																																																<span class="fa-stack">
																	<i class="material-design-bookmark45 fa-stack-1x"></i>
																	<i class="material-design-bookmark45 star fa-stack-1x"></i>
																</span>
																																																																<span class="fa-stack">
																	<i class="material-design-bookmark45 fa-stack-1x"></i>
																	<i class="material-design-bookmark45 star fa-stack-1x"></i>
																</span>
																																																																<span class="fa-stack">
																	<i class="material-design-bookmark45 fa-stack-1x"></i>
																</span>
																																															</div>
																														<button class="product-btn-add" data-toggle="tooltip" title="Add to Cart" type="button" onclick="cart.add('31');">
																<i class="material-design-shopping231"></i>
																<span>Add to Cart</span>
															</button>
															<ul class="product-buttons">
																<li>
																	<button class="product-btn" type="button" data-toggle="tooltip" title="Add to Wishlist" onclick="wishlist.add('31');">
																		<i class="fa fa-heart"></i>
																		<span>Add to Wishlist</span>
																	</button>
																</li>
																<li>
																	<button class="product-btn" type="button" data-toggle="tooltip" title="Add to Compare" onclick="compare.add('31');">
																		<i class="fa fa-exchange"></i>
																		<span>Add to Compare</span>
																	</button>
																</li>
															</ul>
															<div class="clear"></div>
														</div>
														<div class="col-sm-12">
															<div class="quickview_description description">
																
<p>In the modern world, there are dozens of means that can help you achieve a desired look and establish a proper image. One of such means is cosmetics. They include a vast amount of tools designed to enhance your attractiveness. There are also some subcategories of cosmetics that offer specialized care for your skin or that can be used to highlight facial features (your eyes, brows, lips, cheeks etc.) As these products have been widely used since the very beginning of our civilization, today they are available in the greatest variety from lipsticks to face powders with different colors and shapes.</p>
<p>Due to an increasing amount of makeup-related products, we decided to offer our expert help to a lot of people looking for high-quality cosmetics. At our store, you can easily find any type of makeup, hair dye, and deodorants or skincare products to satisfy the most exquisite tastes and needs, both men’s and women’s. As an industry leader in providing top-notch makeup and cosmetic products at affordable price, we can assure you won’t be disappointed by making a purchase at our website.</p>
<p>We care about our customers and their buying experience. That’s why our team pays a lot of attention to your order’s delivery process and provides full support on the stage of choosing a product that would suit you and be an ideal addition to your style. Moreover, our store provides regular discounts to our loyal customers who want to be well informed about the latest trends in cosmetics or the way they can benefit from buying at our store. If you are looking for new makeup tools to fill up your bag or are just wondering what it takes to look beautiful, visit our website and we will be glad to help you!</p>															</div>
														</div>
													</div>
												</div>
											</div>
																																	<div class="image">
												<a class="lazy" style="padding-bottom: 100%" href="https://livedemo00.template-help.com/opencart_59086/index.php?route=product/product&amp;product_id=31">
													<img width="5" height="5" alt="butter LONDON LIPPY Moisture Matte Lipstick" title="butter LONDON LIPPY Moisture Matte Lipstick" class="img-responsive" data-src="image/cache/catalog/products/product_10-5x5.png" src="#"/>
												</a>
												<a class="quickview" data-rel="details" href="#quickview_50010">
													Quick View												</a>
																									<div class="rating">
																																										<span class="fa-stack"> <i class="fa fa-star active fa-stack-1x"></i> </span>
																																																								<span class="fa-stack"> <i class="fa fa-star active fa-stack-1x"></i> </span>
																																																								<span class="fa-stack"> <i class="fa fa-star active fa-stack-1x"></i> </span>
																																																								<span class="fa-stack"> <i class="fa fa-star active fa-stack-1x"></i> </span>
																																																								<span class="fa-stack"> <i class="fa fa-star fa-stack-1x"></i> </span>
																																									</div>
																							</div>
											<div class="caption">
												<div class="name name__aside">
													<a href="https://livedemo00.template-help.com/opencart_59086/index.php?route=product/product&amp;product_id=31">butter LONDON LIPPY Moisture Matte Lipstick</a>
												</div>
																								<div class="price">
																										$70.00																																						</div>
																								<div class="name">
													<a href="https://livedemo00.template-help.com/opencart_59086/index.php?route=product/product&amp;product_id=31">butter LONDON LIPPY Moisture Matte Lipstick</a>
												</div>												
											</div>
											<div class="cart-button">
												<button class="btn-primary" type="button"  onclick="ajaxAdd($(this),31);" >
													<i class="fl-outicons-shopping-cart13"></i> <span>Add to Cart</span>
												</button>
												<div class="clear"></div>
												<button class="product-btn" type="button" onclick="wishlist.add('31');">
													<i class="material-icons-favorite_border"></i> <span>Add to Wishlist</span>
												</button>
												<button class="product-btn" type="button" onclick="compare.add('31');">
													<i class="material-icons-shuffle"></i> <span>Add to Compare</span>
												</button>
											</div>
										</div>
																			</div>
																																				<div class="product-layout col-lg-3 col-md-3 col-sm-3 col-xs-12">
																				<div class="product-thumb transition options">
																						<!-- Product options -->
											<div class="product-option-wrap">
												<div class="product-options form-horizontal">
													<div class="options">
														<a class="ajax-overlay_close" href='#'></a>
														<h3>Available Options</h3>
														<div class="form-group hidden">
															<div class="col-sm-8">
																<input type="text" name="product_id" value="49" class="form-control"/>
															</div>
														</div>
																																										<div class="form-group required">
															<label class="control-label col-sm-12" for="input-option3705002">
																Size															</label>
															<div class="col-sm-12">
																<select name="option[37]" id="input-option3705002" class="form-control">
																	<option value="">--- Please Select ---</option>
																																		<option value="83">
																		Small																																				(+$20.00)
																																			</option>
																																		<option value="84">
																		Medium																																				(+$15.00)
																																			</option>
																																		<option value="85">
																		Large																																				(+$10.00)
																																			</option>
																																	</select>
															</div>
														</div>
																																																																																																																																																																																																				<div class="form-group required">
															<label class="control-label col-sm-12">Color</label>
															<div class="col-sm-12">
																<div id="input-option3805002">
																																		<div class="radio">
																		<label for="option[388605002]">
																			<input type="radio" hidden name="option[38]" id="option[388605002]" value="86"/>
																			red																																						(+$10.00)
																																					</label>
																	</div>
																																	</div>
															</div>
														</div>
																																																																																																																																																										<button class="product-btn-add" type="button" onclick="cart.addPopup($(this),'49');">
															<i class="material-design-shopping231"></i>
															<span>Add to Cart</span>
														</button>
													</div>
												</div>
											</div>
																						<div class="quick_info">
												<div id="quickview_50020" class="quickview-style">
													<div>
														<div class="left col-sm-4">
															<div class="quickview_image image">
																<a href="https://livedemo00.template-help.com/opencart_59086/index.php?route=product/product&amp;product_id=49"><img width="5" height="5" alt="VERAttiva Face Tonic Rinse and Refresher 7-Ounce" title="VERAttiva Face Tonic Rinse and Refresher 7-Ounce" class="img-responsive" src="image/cache/catalog/products/product_55-5x5.png"/></a>
															</div>
														</div>
														<div class="right col-sm-8">
															<h2>VERAttiva Face Tonic Rinse and Refresher 7-Ounce</h2>
															<div class="inf">
																																<p class="quickview_manufacture manufacture manufacture">Brand:																	<a href="https://livedemo00.template-help.com/opencart_59086/index.php?route=product/manufacturer/info&amp;manufacturer_id=33">Clarins</a>
																</p>
																																																<p class="product_model model">Model: Deluxe</p>
																																																<div class="price">
																																		<span class="price-new">$56.00</span>
																	<span class="price-old">$70.00</span>
																																																		</div>
																															</div>
																														<div class="rating">
																																																<span class="fa-stack">
																	<i class="material-design-bookmark45 fa-stack-1x"></i>
																	<i class="material-design-bookmark45 star fa-stack-1x"></i>
																</span>
																																																																<span class="fa-stack">
																	<i class="material-design-bookmark45 fa-stack-1x"></i>
																	<i class="material-design-bookmark45 star fa-stack-1x"></i>
																</span>
																																																																<span class="fa-stack">
																	<i class="material-design-bookmark45 fa-stack-1x"></i>
																	<i class="material-design-bookmark45 star fa-stack-1x"></i>
																</span>
																																																																<span class="fa-stack">
																	<i class="material-design-bookmark45 fa-stack-1x"></i>
																	<i class="material-design-bookmark45 star fa-stack-1x"></i>
																</span>
																																																																<span class="fa-stack">
																	<i class="material-design-bookmark45 fa-stack-1x"></i>
																	<i class="material-design-bookmark45 star fa-stack-1x"></i>
																</span>
																																															</div>
																														<button class="product-btn-add" data-toggle="tooltip" title="Add to Cart" type="button" onclick="cart.add('49');">
																<i class="material-design-shopping231"></i>
																<span>Add to Cart</span>
															</button>
															<ul class="product-buttons">
																<li>
																	<button class="product-btn" type="button" data-toggle="tooltip" title="Add to Wishlist" onclick="wishlist.add('49');">
																		<i class="fa fa-heart"></i>
																		<span>Add to Wishlist</span>
																	</button>
																</li>
																<li>
																	<button class="product-btn" type="button" data-toggle="tooltip" title="Add to Compare" onclick="compare.add('49');">
																		<i class="fa fa-exchange"></i>
																		<span>Add to Compare</span>
																	</button>
																</li>
															</ul>
															<div class="clear"></div>
														</div>
														<div class="col-sm-12">
															<div class="quickview_description description">
																
<p>In the modern world, there are dozens of means that can help you achieve a desired look and establish a proper image. One of such means is cosmetics. They include a vast amount of tools designed to enhance your attractiveness. There are also some subcategories of cosmetics that offer specialized care for your skin or that can be used to highlight facial features (your eyes, brows, lips, cheeks etc.) As these products have been widely used since the very beginning of our civilization, today they are available in the greatest variety from lipsticks to face powders with different colors and shapes.</p>
<p>Due to an increasing amount of makeup-related products, we decided to offer our expert help to a lot of people looking for high-quality cosmetics. At our store, you can easily find any type of makeup, hair dye, and deodorants or skincare products to satisfy the most exquisite tastes and needs, both men’s and women’s. As an industry leader in providing top-notch makeup and cosmetic products at affordable price, we can assure you won’t be disappointed by making a purchase at our website.</p>
<p>We care about our customers and their buying experience. That’s why our team pays a lot of attention to your order’s delivery process and provides full support on the stage of choosing a product that would suit you and be an ideal addition to your style. Moreover, our store provides regular discounts to our loyal customers who want to be well informed about the latest trends in cosmetics or the way they can benefit from buying at our store. If you are looking for new makeup tools to fill up your bag or are just wondering what it takes to look beautiful, visit our website and we will be glad to help you!</p>															</div>
														</div>
													</div>
												</div>
											</div>
																																	<div class="sale">
												<span>Sale!</span>
											</div>
																																																							<div class="image">
												<a class="lazy" style="padding-bottom: 100%" href="https://livedemo00.template-help.com/opencart_59086/index.php?route=product/product&amp;product_id=49">
													<img width="5" height="5" alt="VERAttiva Face Tonic Rinse and Refresher 7-Ounce" title="VERAttiva Face Tonic Rinse and Refresher 7-Ounce" class="img-responsive" data-src="image/cache/catalog/products/product_55-5x5.png" src="#"/>
												</a>
												<a class="quickview" data-rel="details" href="#quickview_50020">
													Quick View												</a>
																									<div class="rating">
																																										<span class="fa-stack"> <i class="fa fa-star active fa-stack-1x"></i> </span>
																																																								<span class="fa-stack"> <i class="fa fa-star active fa-stack-1x"></i> </span>
																																																								<span class="fa-stack"> <i class="fa fa-star active fa-stack-1x"></i> </span>
																																																								<span class="fa-stack"> <i class="fa fa-star active fa-stack-1x"></i> </span>
																																																								<span class="fa-stack"> <i class="fa fa-star active fa-stack-1x"></i> </span>
																																									</div>
																							</div>
											<div class="caption">
												<div class="name name__aside">
													<a href="https://livedemo00.template-help.com/opencart_59086/index.php?route=product/product&amp;product_id=49">VERAttiva Face Tonic Rinse and Refresher 7-Ounce</a>
												</div>
																								<div class="price">
																										<span class="price-new">$56.00</span>
													<span class="price-old">$70.00</span>
																																						</div>
																								<div class="name">
													<a href="https://livedemo00.template-help.com/opencart_59086/index.php?route=product/product&amp;product_id=49">VERAttiva Face Tonic Rinse and Refresher 7-Ounce</a>
												</div>												
											</div>
											<div class="cart-button">
												<button class="btn-primary" type="button"  onclick="ajaxAdd($(this),49);" >
													<i class="fl-outicons-shopping-cart13"></i> <span>Add to Cart</span>
												</button>
												<div class="clear"></div>
												<button class="product-btn" type="button" onclick="wishlist.add('49');">
													<i class="material-icons-favorite_border"></i> <span>Add to Wishlist</span>
												</button>
												<button class="product-btn" type="button" onclick="compare.add('49');">
													<i class="material-icons-shuffle"></i> <span>Add to Compare</span>
												</button>
											</div>
										</div>
																			</div>
																																				<div class="product-layout col-lg-3 col-md-3 col-sm-3 col-xs-12">
																				<div class="product-thumb transition options">
																						<!-- Product options -->
											<div class="product-option-wrap">
												<div class="product-options form-horizontal">
													<div class="options">
														<a class="ajax-overlay_close" href='#'></a>
														<h3>Available Options</h3>
														<div class="form-group hidden">
															<div class="col-sm-8">
																<input type="text" name="product_id" value="43" class="form-control"/>
															</div>
														</div>
																																										<div class="form-group required">
															<label class="control-label col-sm-12" for="input-option2705003">
																Size															</label>
															<div class="col-sm-12">
																<select name="option[27]" id="input-option2705003" class="form-control">
																	<option value="">--- Please Select ---</option>
																																		<option value="61">
																		Small																																				(+$20.00)
																																			</option>
																																		<option value="62">
																		Medium																																				(+$15.00)
																																			</option>
																																		<option value="63">
																		Large																																				(+$10.00)
																																			</option>
																																	</select>
															</div>
														</div>
																																																																																																																																																																																																				<div class="form-group required">
															<label class="control-label col-sm-12">Color</label>
															<div class="col-sm-12">
																<div id="input-option2805003">
																																		<div class="radio">
																		<label for="option[286405003]">
																			<input type="radio" hidden name="option[28]" id="option[286405003]" value="64"/>
																			red																																						(+$10.00)
																																					</label>
																	</div>
																																	</div>
															</div>
														</div>
																																																																																																																																																										<button class="product-btn-add" type="button" onclick="cart.addPopup($(this),'43');">
															<i class="material-design-shopping231"></i>
															<span>Add to Cart</span>
														</button>
													</div>
												</div>
											</div>
																						<div class="quick_info">
												<div id="quickview_50030" class="quickview-style">
													<div>
														<div class="left col-sm-4">
															<div class="quickview_image image">
																<a href="https://livedemo00.template-help.com/opencart_59086/index.php?route=product/product&amp;product_id=43"><img width="5" height="5" alt="Anti-Aging Daily Moisturizer" title="Anti-Aging Daily Moisturizer" class="img-responsive" src="image/cache/catalog/products/product_37-5x5.png"/></a>
															</div>
														</div>
														<div class="right col-sm-8">
															<h2>Anti-Aging Daily Moisturizer</h2>
															<div class="inf">
																																<p class="quickview_manufacture manufacture manufacture">Brand:																	<a href="https://livedemo00.template-help.com/opencart_59086/index.php?route=product/manufacturer/info&amp;manufacturer_id=27">Nivea</a>
																</p>
																																																<p class="product_model model">Model: Premium</p>
																																																<div class="price">
																																		<span class="price-new">$64.00</span>
																	<span class="price-old">$80.00</span>
																																																		</div>
																															</div>
																														<div class="rating">
																																																<span class="fa-stack">
																	<i class="material-design-bookmark45 fa-stack-1x"></i>
																	<i class="material-design-bookmark45 star fa-stack-1x"></i>
																</span>
																																																																<span class="fa-stack">
																	<i class="material-design-bookmark45 fa-stack-1x"></i>
																	<i class="material-design-bookmark45 star fa-stack-1x"></i>
																</span>
																																																																<span class="fa-stack">
																	<i class="material-design-bookmark45 fa-stack-1x"></i>
																	<i class="material-design-bookmark45 star fa-stack-1x"></i>
																</span>
																																																																<span class="fa-stack">
																	<i class="material-design-bookmark45 fa-stack-1x"></i>
																	<i class="material-design-bookmark45 star fa-stack-1x"></i>
																</span>
																																																																<span class="fa-stack">
																	<i class="material-design-bookmark45 fa-stack-1x"></i>
																</span>
																																															</div>
																														<button class="product-btn-add" data-toggle="tooltip" title="Add to Cart" type="button" onclick="cart.add('43');">
																<i class="material-design-shopping231"></i>
																<span>Add to Cart</span>
															</button>
															<ul class="product-buttons">
																<li>
																	<button class="product-btn" type="button" data-toggle="tooltip" title="Add to Wishlist" onclick="wishlist.add('43');">
																		<i class="fa fa-heart"></i>
																		<span>Add to Wishlist</span>
																	</button>
																</li>
																<li>
																	<button class="product-btn" type="button" data-toggle="tooltip" title="Add to Compare" onclick="compare.add('43');">
																		<i class="fa fa-exchange"></i>
																		<span>Add to Compare</span>
																	</button>
																</li>
															</ul>
															<div class="clear"></div>
														</div>
														<div class="col-sm-12">
															<div class="quickview_description description">
																
<p>In the modern world, there are dozens of means that can help you achieve a desired look and establish a proper image. One of such means is cosmetics. They include a vast amount of tools designed to enhance your attractiveness. There are also some subcategories of cosmetics that offer specialized care for your skin or that can be used to highlight facial features (your eyes, brows, lips, cheeks etc.) As these products have been widely used since the very beginning of our civilization, today they are available in the greatest variety from lipsticks to face powders with different colors and shapes.</p>
<p>Due to an increasing amount of makeup-related products, we decided to offer our expert help to a lot of people looking for high-quality cosmetics. At our store, you can easily find any type of makeup, hair dye, and deodorants or skincare products to satisfy the most exquisite tastes and needs, both men’s and women’s. As an industry leader in providing top-notch makeup and cosmetic products at affordable price, we can assure you won’t be disappointed by making a purchase at our website.</p>
<p>We care about our customers and their buying experience. That’s why our team pays a lot of attention to your order’s delivery process and provides full support on the stage of choosing a product that would suit you and be an ideal addition to your style. Moreover, our store provides regular discounts to our loyal customers who want to be well informed about the latest trends in cosmetics or the way they can benefit from buying at our store. If you are looking for new makeup tools to fill up your bag or are just wondering what it takes to look beautiful, visit our website and we will be glad to help you!</p>															</div>
														</div>
													</div>
												</div>
											</div>
																																	<div class="sale">
												<span>Sale!</span>
											</div>
																																																							<div class="image">
												<a class="lazy" style="padding-bottom: 100%" href="https://livedemo00.template-help.com/opencart_59086/index.php?route=product/product&amp;product_id=43">
													<img width="5" height="5" alt="Anti-Aging Daily Moisturizer" title="Anti-Aging Daily Moisturizer" class="img-responsive" data-src="image/cache/catalog/products/product_37-5x5.png" src="#"/>
												</a>
												<a class="quickview" data-rel="details" href="#quickview_50030">
													Quick View												</a>
																									<div class="rating">
																																										<span class="fa-stack"> <i class="fa fa-star active fa-stack-1x"></i> </span>
																																																								<span class="fa-stack"> <i class="fa fa-star active fa-stack-1x"></i> </span>
																																																								<span class="fa-stack"> <i class="fa fa-star active fa-stack-1x"></i> </span>
																																																								<span class="fa-stack"> <i class="fa fa-star active fa-stack-1x"></i> </span>
																																																								<span class="fa-stack"> <i class="fa fa-star fa-stack-1x"></i> </span>
																																									</div>
																							</div>
											<div class="caption">
												<div class="name name__aside">
													<a href="https://livedemo00.template-help.com/opencart_59086/index.php?route=product/product&amp;product_id=43">Anti-Aging Daily Moisturizer</a>
												</div>
																								<div class="price">
																										<span class="price-new">$64.00</span>
													<span class="price-old">$80.00</span>
																																						</div>
																								<div class="name">
													<a href="https://livedemo00.template-help.com/opencart_59086/index.php?route=product/product&amp;product_id=43">Anti-Aging Daily Moisturizer</a>
												</div>												
											</div>
											<div class="cart-button">
												<button class="btn-primary" type="button"  onclick="ajaxAdd($(this),43);" >
													<i class="fl-outicons-shopping-cart13"></i> <span>Add to Cart</span>
												</button>
												<div class="clear"></div>
												<button class="product-btn" type="button" onclick="wishlist.add('43');">
													<i class="material-icons-favorite_border"></i> <span>Add to Wishlist</span>
												</button>
												<button class="product-btn" type="button" onclick="compare.add('43');">
													<i class="material-icons-shuffle"></i> <span>Add to Compare</span>
												</button>
											</div>
										</div>
																			</div>
																																				<div class="product-layout col-lg-3 col-md-3 col-sm-3 col-xs-12">
																				<div class="product-thumb transition options">
																						<!-- Product options -->
											<div class="product-option-wrap">
												<div class="product-options form-horizontal">
													<div class="options">
														<a class="ajax-overlay_close" href='#'></a>
														<h3>Available Options</h3>
														<div class="form-group hidden">
															<div class="col-sm-8">
																<input type="text" name="product_id" value="36" class="form-control"/>
															</div>
														</div>
																																										<div class="form-group required">
															<label class="control-label col-sm-12" for="input-option1705004">
																Size															</label>
															<div class="col-sm-12">
																<select name="option[17]" id="input-option1705004" class="form-control">
																	<option value="">--- Please Select ---</option>
																																		<option value="40">
																		Small																																				(+$20.00)
																																			</option>
																																		<option value="41">
																		Medium																																				(+$15.00)
																																			</option>
																																		<option value="42">
																		Large																																				(+$10.00)
																																			</option>
																																	</select>
															</div>
														</div>
																																																																																																																																																																																																				<div class="form-group required">
															<label class="control-label col-sm-12">Color</label>
															<div class="col-sm-12">
																<div id="input-option1805004">
																																		<div class="radio">
																		<label for="option[184505004]">
																			<input type="radio" hidden name="option[18]" id="option[184505004]" value="45"/>
																			black																																						(+$20.00)
																																					</label>
																	</div>
																																		<div class="radio">
																		<label for="option[184405004]">
																			<input type="radio" hidden name="option[18]" id="option[184405004]" value="44"/>
																			black																																						(+$15.00)
																																					</label>
																	</div>
																																		<div class="radio">
																		<label for="option[184305004]">
																			<input type="radio" hidden name="option[18]" id="option[184305004]" value="43"/>
																			red																																						(+$10.00)
																																					</label>
																	</div>
																																	</div>
															</div>
														</div>
																																																																																																																																																										<button class="product-btn-add" type="button" onclick="cart.addPopup($(this),'36');">
															<i class="material-design-shopping231"></i>
															<span>Add to Cart</span>
														</button>
													</div>
												</div>
											</div>
																						<div class="quick_info">
												<div id="quickview_50040" class="quickview-style">
													<div>
														<div class="left col-sm-4">
															<div class="quickview_image image">
																<a href="https://livedemo00.template-help.com/opencart_59086/index.php?route=product/product&amp;product_id=36"><img width="5" height="5" alt="NIVEA Body Creme 13_5 Ounce" title="NIVEA Body Creme 13_5 Ounce" class="img-responsive" src="image/cache/catalog/products/product_25-5x5.png"/></a>
															</div>
														</div>
														<div class="right col-sm-8">
															<h2>NIVEA Body Creme 13_5 Ounce</h2>
															<div class="inf">
																																<p class="quickview_manufacture manufacture manufacture">Brand:																	<a href="https://livedemo00.template-help.com/opencart_59086/index.php?route=product/manufacturer/info&amp;manufacturer_id=33">Clarins</a>
																</p>
																																																<p class="product_model model">Model: Deluxe</p>
																																																<div class="price">
																																		$60.00																																																		</div>
																															</div>
																														<button class="product-btn-add" data-toggle="tooltip" title="Add to Cart" type="button" onclick="cart.add('36');">
																<i class="material-design-shopping231"></i>
																<span>Add to Cart</span>
															</button>
															<ul class="product-buttons">
																<li>
																	<button class="product-btn" type="button" data-toggle="tooltip" title="Add to Wishlist" onclick="wishlist.add('36');">
																		<i class="fa fa-heart"></i>
																		<span>Add to Wishlist</span>
																	</button>
																</li>
																<li>
																	<button class="product-btn" type="button" data-toggle="tooltip" title="Add to Compare" onclick="compare.add('36');">
																		<i class="fa fa-exchange"></i>
																		<span>Add to Compare</span>
																	</button>
																</li>
															</ul>
															<div class="clear"></div>
														</div>
														<div class="col-sm-12">
															<div class="quickview_description description">
																
<p>In the modern world, there are dozens of means that can help you achieve a desired look and establish a proper image. One of such means is cosmetics. They include a vast amount of tools designed to enhance your attractiveness. There are also some subcategories of cosmetics that offer specialized care for your skin or that can be used to highlight facial features (your eyes, brows, lips, cheeks etc.) As these products have been widely used since the very beginning of our civilization, today they are available in the greatest variety from lipsticks to face powders with different colors and shapes.</p>
<p>Due to an increasing amount of makeup-related products, we decided to offer our expert help to a lot of people looking for high-quality cosmetics. At our store, you can easily find any type of makeup, hair dye, and deodorants or skincare products to satisfy the most exquisite tastes and needs, both men’s and women’s. As an industry leader in providing top-notch makeup and cosmetic products at affordable price, we can assure you won’t be disappointed by making a purchase at our website.</p>
<p>We care about our customers and their buying experience. That’s why our team pays a lot of attention to your order’s delivery process and provides full support on the stage of choosing a product that would suit you and be an ideal addition to your style. Moreover, our store provides regular discounts to our loyal customers who want to be well informed about the latest trends in cosmetics or the way they can benefit from buying at our store. If you are looking for new makeup tools to fill up your bag or are just wondering what it takes to look beautiful, visit our website and we will be glad to help you!</p>															</div>
														</div>
													</div>
												</div>
											</div>
																																	<div class="image">
												<a class="lazy" style="padding-bottom: 100%" href="https://livedemo00.template-help.com/opencart_59086/index.php?route=product/product&amp;product_id=36">
													<img width="5" height="5" alt="NIVEA Body Creme 13_5 Ounce" title="NIVEA Body Creme 13_5 Ounce" class="img-responsive" data-src="image/cache/catalog/products/product_25-5x5.png" src="#"/>
												</a>
												<a class="quickview" data-rel="details" href="#quickview_50040">
													Quick View												</a>
																							</div>
											<div class="caption">
												<div class="name name__aside">
													<a href="https://livedemo00.template-help.com/opencart_59086/index.php?route=product/product&amp;product_id=36">NIVEA Body Creme 13_5 Ounce</a>
												</div>
																								<div class="price">
																										$60.00																																						</div>
																								<div class="name">
													<a href="https://livedemo00.template-help.com/opencart_59086/index.php?route=product/product&amp;product_id=36">NIVEA Body Creme 13_5 Ounce</a>
												</div>												
											</div>
											<div class="cart-button">
												<button class="btn-primary" type="button"  onclick="ajaxAdd($(this),36);" >
													<i class="fl-outicons-shopping-cart13"></i> <span>Add to Cart</span>
												</button>
												<div class="clear"></div>
												<button class="product-btn" type="button" onclick="wishlist.add('36');">
													<i class="material-icons-favorite_border"></i> <span>Add to Wishlist</span>
												</button>
												<button class="product-btn" type="button" onclick="compare.add('36');">
													<i class="material-icons-shuffle"></i> <span>Add to Compare</span>
												</button>
											</div>
										</div>
																			</div>
																																				<div class="product-layout col-lg-3 col-md-3 col-sm-3 col-xs-12">
																				<div class="product-thumb transition options">
																						<!-- Product options -->
											<div class="product-option-wrap">
												<div class="product-options form-horizontal">
													<div class="options">
														<a class="ajax-overlay_close" href='#'></a>
														<h3>Available Options</h3>
														<div class="form-group hidden">
															<div class="col-sm-8">
																<input type="text" name="product_id" value="41" class="form-control"/>
															</div>
														</div>
																																										<div class="form-group required">
															<label class="control-label col-sm-12" for="input-option2105005">
																Size															</label>
															<div class="col-sm-12">
																<select name="option[21]" id="input-option2105005" class="form-control">
																	<option value="">--- Please Select ---</option>
																																		<option value="52">
																		Small																																				(+$20.00)
																																			</option>
																																		<option value="53">
																		Medium																																				(+$15.00)
																																			</option>
																																		<option value="54">
																		Large																																				(+$10.00)
																																			</option>
																																	</select>
															</div>
														</div>
																																																																																																																																																																								<button class="product-btn-add" type="button" onclick="cart.addPopup($(this),'41');">
															<i class="material-design-shopping231"></i>
															<span>Add to Cart</span>
														</button>
													</div>
												</div>
											</div>
																						<div class="quick_info">
												<div id="quickview_50050" class="quickview-style">
													<div>
														<div class="left col-sm-4">
															<div class="quickview_image image">
																<a href="https://livedemo00.template-help.com/opencart_59086/index.php?route=product/product&amp;product_id=41"><img width="5" height="5" alt="All Day UV Moisture Cream SPF 15" title="All Day UV Moisture Cream SPF 15" class="img-responsive" src="image/cache/catalog/products/product_31-5x5.png"/></a>
															</div>
														</div>
														<div class="right col-sm-8">
															<h2>All Day UV Moisture Cream SPF 15</h2>
															<div class="inf">
																																<p class="quickview_manufacture manufacture manufacture">Brand:																	<a href="https://livedemo00.template-help.com/opencart_59086/index.php?route=product/manufacturer/info&amp;manufacturer_id=25">CeraVe</a>
																</p>
																																																<p class="product_model model">Model: Premium</p>
																																																<div class="price">
																																		<span class="price-new">$44.00</span>
																	<span class="price-old">$55.00</span>
																																																		</div>
																															</div>
																														<button class="product-btn-add" data-toggle="tooltip" title="Add to Cart" type="button" onclick="cart.add('41');">
																<i class="material-design-shopping231"></i>
																<span>Add to Cart</span>
															</button>
															<ul class="product-buttons">
																<li>
																	<button class="product-btn" type="button" data-toggle="tooltip" title="Add to Wishlist" onclick="wishlist.add('41');">
																		<i class="fa fa-heart"></i>
																		<span>Add to Wishlist</span>
																	</button>
																</li>
																<li>
																	<button class="product-btn" type="button" data-toggle="tooltip" title="Add to Compare" onclick="compare.add('41');">
																		<i class="fa fa-exchange"></i>
																		<span>Add to Compare</span>
																	</button>
																</li>
															</ul>
															<div class="clear"></div>
														</div>
														<div class="col-sm-12">
															<div class="quickview_description description">
																
<p>In the modern world, there are dozens of means that can help you achieve a desired look and establish a proper image. One of such means is cosmetics. They include a vast amount of tools designed to enhance your attractiveness. There are also some subcategories of cosmetics that offer specialized care for your skin or that can be used to highlight facial features (your eyes, brows, lips, cheeks etc.) As these products have been widely used since the very beginning of our civilization, today they are available in the greatest variety from lipsticks to face powders with different colors and shapes.</p>
<p>Due to an increasing amount of makeup-related products, we decided to offer our expert help to a lot of people looking for high-quality cosmetics. At our store, you can easily find any type of makeup, hair dye, and deodorants or skincare products to satisfy the most exquisite tastes and needs, both men’s and women’s. As an industry leader in providing top-notch makeup and cosmetic products at affordable price, we can assure you won’t be disappointed by making a purchase at our website.</p>
<p>We care about our customers and their buying experience. That’s why our team pays a lot of attention to your order’s delivery process and provides full support on the stage of choosing a product that would suit you and be an ideal addition to your style. Moreover, our store provides regular discounts to our loyal customers who want to be well informed about the latest trends in cosmetics or the way they can benefit from buying at our store. If you are looking for new makeup tools to fill up your bag or are just wondering what it takes to look beautiful, visit our website and we will be glad to help you!</p>															</div>
														</div>
													</div>
												</div>
											</div>
																																	<div class="sale">
												<span>Sale!</span>
											</div>
																																																							<div class="image">
												<a class="lazy" style="padding-bottom: 100%" href="https://livedemo00.template-help.com/opencart_59086/index.php?route=product/product&amp;product_id=41">
													<img width="5" height="5" alt="All Day UV Moisture Cream SPF 15" title="All Day UV Moisture Cream SPF 15" class="img-responsive" data-src="image/cache/catalog/products/product_31-5x5.png" src="#"/>
												</a>
												<a class="quickview" data-rel="details" href="#quickview_50050">
													Quick View												</a>
																							</div>
											<div class="caption">
												<div class="name name__aside">
													<a href="https://livedemo00.template-help.com/opencart_59086/index.php?route=product/product&amp;product_id=41">All Day UV Moisture Cream SPF 15</a>
												</div>
																								<div class="price">
																										<span class="price-new">$44.00</span>
													<span class="price-old">$55.00</span>
																																						</div>
																								<div class="name">
													<a href="https://livedemo00.template-help.com/opencart_59086/index.php?route=product/product&amp;product_id=41">All Day UV Moisture Cream SPF 15</a>
												</div>												
											</div>
											<div class="cart-button">
												<button class="btn-primary" type="button"  onclick="ajaxAdd($(this),41);" >
													<i class="fl-outicons-shopping-cart13"></i> <span>Add to Cart</span>
												</button>
												<div class="clear"></div>
												<button class="product-btn" type="button" onclick="wishlist.add('41');">
													<i class="material-icons-favorite_border"></i> <span>Add to Wishlist</span>
												</button>
												<button class="product-btn" type="button" onclick="compare.add('41');">
													<i class="material-icons-shuffle"></i> <span>Add to Compare</span>
												</button>
											</div>
										</div>
																			</div>
																																				<div class="product-layout col-lg-3 col-md-3 col-sm-3 col-xs-12">
																				<div class="product-thumb transition options">
																						<!-- Product options -->
											<div class="product-option-wrap">
												<div class="product-options form-horizontal">
													<div class="options">
														<a class="ajax-overlay_close" href='#'></a>
														<h3>Available Options</h3>
														<div class="form-group hidden">
															<div class="col-sm-8">
																<input type="text" name="product_id" value="30" class="form-control"/>
															</div>
														</div>
																																										<div class="form-group required">
															<label class="control-label col-sm-12" for="input-option505006">
																Size															</label>
															<div class="col-sm-12">
																<select name="option[5]" id="input-option505006" class="form-control">
																	<option value="">--- Please Select ---</option>
																																		<option value="11">
																		Small																																				(+$10.00)
																																			</option>
																																		<option value="12">
																		Medium																																				(+$15.00)
																																			</option>
																																		<option value="13">
																		Large																																				(+$20.00)
																																			</option>
																																	</select>
															</div>
														</div>
																																																																																																																																																																																																				<div class="form-group required">
															<label class="control-label col-sm-12">Color</label>
															<div class="col-sm-12">
																<div id="input-option605006">
																																		<div class="radio">
																		<label for="option[61605006]">
																			<input type="radio" hidden name="option[6]" id="option[61605006]" value="16"/>
																			black																																						(+$10.00)
																																					</label>
																	</div>
																																		<div class="radio">
																		<label for="option[61505006]">
																			<input type="radio" hidden name="option[6]" id="option[61505006]" value="15"/>
																			black																																						(+$15.00)
																																					</label>
																	</div>
																																		<div class="radio">
																		<label for="option[61405006]">
																			<input type="radio" hidden name="option[6]" id="option[61405006]" value="14"/>
																			red																																						(+$20.00)
																																					</label>
																	</div>
																																	</div>
															</div>
														</div>
																																																																																																																																																										<button class="product-btn-add" type="button" onclick="cart.addPopup($(this),'30');">
															<i class="material-design-shopping231"></i>
															<span>Add to Cart</span>
														</button>
													</div>
												</div>
											</div>
																						<div class="quick_info">
												<div id="quickview_50060" class="quickview-style">
													<div>
														<div class="left col-sm-4">
															<div class="quickview_image image">
																<a href="https://livedemo00.template-help.com/opencart_59086/index.php?route=product/product&amp;product_id=30"><img width="5" height="5" alt="Immediato Spa Soothing Tonic" title="Immediato Spa Soothing Tonic" class="img-responsive" src="image/cache/catalog/products/product_7-5x5.png"/></a>
															</div>
														</div>
														<div class="right col-sm-8">
															<h2>Immediato Spa Soothing Tonic</h2>
															<div class="inf">
																																<p class="quickview_manufacture manufacture manufacture">Brand:																	<a href="https://livedemo00.template-help.com/opencart_59086/index.php?route=product/manufacturer/info&amp;manufacturer_id=27">Nivea</a>
																</p>
																																																<p class="product_model model">Model: Premium</p>
																																																<div class="price">
																																		<span class="price-new">$60.00</span>
																	<span class="price-old">$75.00</span>
																																																		</div>
																															</div>
																														<div class="rating">
																																																<span class="fa-stack">
																	<i class="material-design-bookmark45 fa-stack-1x"></i>
																	<i class="material-design-bookmark45 star fa-stack-1x"></i>
																</span>
																																																																<span class="fa-stack">
																	<i class="material-design-bookmark45 fa-stack-1x"></i>
																	<i class="material-design-bookmark45 star fa-stack-1x"></i>
																</span>
																																																																<span class="fa-stack">
																	<i class="material-design-bookmark45 fa-stack-1x"></i>
																	<i class="material-design-bookmark45 star fa-stack-1x"></i>
																</span>
																																																																<span class="fa-stack">
																	<i class="material-design-bookmark45 fa-stack-1x"></i>
																	<i class="material-design-bookmark45 star fa-stack-1x"></i>
																</span>
																																																																<span class="fa-stack">
																	<i class="material-design-bookmark45 fa-stack-1x"></i>
																	<i class="material-design-bookmark45 star fa-stack-1x"></i>
																</span>
																																															</div>
																														<button class="product-btn-add" data-toggle="tooltip" title="Add to Cart" type="button" onclick="cart.add('30');">
																<i class="material-design-shopping231"></i>
																<span>Add to Cart</span>
															</button>
															<ul class="product-buttons">
																<li>
																	<button class="product-btn" type="button" data-toggle="tooltip" title="Add to Wishlist" onclick="wishlist.add('30');">
																		<i class="fa fa-heart"></i>
																		<span>Add to Wishlist</span>
																	</button>
																</li>
																<li>
																	<button class="product-btn" type="button" data-toggle="tooltip" title="Add to Compare" onclick="compare.add('30');">
																		<i class="fa fa-exchange"></i>
																		<span>Add to Compare</span>
																	</button>
																</li>
															</ul>
															<div class="clear"></div>
														</div>
														<div class="col-sm-12">
															<div class="quickview_description description">
																
<p>In the modern world, there are dozens of means that can help you achieve a desired look and establish a proper image. One of such means is cosmetics. They include a vast amount of tools designed to enhance your attractiveness. There are also some subcategories of cosmetics that offer specialized care for your skin or that can be used to highlight facial features (your eyes, brows, lips, cheeks etc.) As these products have been widely used since the very beginning of our civilization, today they are available in the greatest variety from lipsticks to face powders with different colors and shapes.</p>
<p>Due to an increasing amount of makeup-related products, we decided to offer our expert help to a lot of people looking for high-quality cosmetics. At our store, you can easily find any type of makeup, hair dye, and deodorants or skincare products to satisfy the most exquisite tastes and needs, both men’s and women’s. As an industry leader in providing top-notch makeup and cosmetic products at affordable price, we can assure you won’t be disappointed by making a purchase at our website.</p>
<p>We care about our customers and their buying experience. That’s why our team pays a lot of attention to your order’s delivery process and provides full support on the stage of choosing a product that would suit you and be an ideal addition to your style. Moreover, our store provides regular discounts to our loyal customers who want to be well informed about the latest trends in cosmetics or the way they can benefit from buying at our store. If you are looking for new makeup tools to fill up your bag or are just wondering what it takes to look beautiful, visit our website and we will be glad to help you!</p>															</div>
														</div>
													</div>
												</div>
											</div>
																																	<div class="sale">
												<span>Sale!</span>
											</div>
																																																							<div class="image">
												<a class="lazy" style="padding-bottom: 100%" href="https://livedemo00.template-help.com/opencart_59086/index.php?route=product/product&amp;product_id=30">
													<img width="5" height="5" alt="Immediato Spa Soothing Tonic" title="Immediato Spa Soothing Tonic" class="img-responsive" data-src="image/cache/catalog/products/product_7-5x5.png" src="#"/>
												</a>
												<a class="quickview" data-rel="details" href="#quickview_50060">
													Quick View												</a>
																									<div class="rating">
																																										<span class="fa-stack"> <i class="fa fa-star active fa-stack-1x"></i> </span>
																																																								<span class="fa-stack"> <i class="fa fa-star active fa-stack-1x"></i> </span>
																																																								<span class="fa-stack"> <i class="fa fa-star active fa-stack-1x"></i> </span>
																																																								<span class="fa-stack"> <i class="fa fa-star active fa-stack-1x"></i> </span>
																																																								<span class="fa-stack"> <i class="fa fa-star active fa-stack-1x"></i> </span>
																																									</div>
																							</div>
											<div class="caption">
												<div class="name name__aside">
													<a href="https://livedemo00.template-help.com/opencart_59086/index.php?route=product/product&amp;product_id=30">Immediato Spa Soothing Tonic</a>
												</div>
																								<div class="price">
																										<span class="price-new">$60.00</span>
													<span class="price-old">$75.00</span>
																																						</div>
																								<div class="name">
													<a href="https://livedemo00.template-help.com/opencart_59086/index.php?route=product/product&amp;product_id=30">Immediato Spa Soothing Tonic</a>
												</div>												
											</div>
											<div class="cart-button">
												<button class="btn-primary" type="button"  onclick="ajaxAdd($(this),30);" >
													<i class="fl-outicons-shopping-cart13"></i> <span>Add to Cart</span>
												</button>
												<div class="clear"></div>
												<button class="product-btn" type="button" onclick="wishlist.add('30');">
													<i class="material-icons-favorite_border"></i> <span>Add to Wishlist</span>
												</button>
												<button class="product-btn" type="button" onclick="compare.add('30');">
													<i class="material-icons-shuffle"></i> <span>Add to Compare</span>
												</button>
											</div>
										</div>
																			</div>
																																				<div class="product-layout col-lg-3 col-md-3 col-sm-3 col-xs-12">
																				<div class="product-thumb transition options">
																						<!-- Product options -->
											<div class="product-option-wrap">
												<div class="product-options form-horizontal">
													<div class="options">
														<a class="ajax-overlay_close" href='#'></a>
														<h3>Available Options</h3>
														<div class="form-group hidden">
															<div class="col-sm-8">
																<input type="text" name="product_id" value="50" class="form-control"/>
															</div>
														</div>
																																										<div class="form-group required">
															<label class="control-label col-sm-12" for="input-option3905007">
																Size															</label>
															<div class="col-sm-12">
																<select name="option[39]" id="input-option3905007" class="form-control">
																	<option value="">--- Please Select ---</option>
																																		<option value="87">
																		Small																																				(+$15.00)
																																			</option>
																																		<option value="88">
																		Medium																																				(+$10.00)
																																			</option>
																																		<option value="89">
																		Large																																				(+$5.00)
																																			</option>
																																	</select>
															</div>
														</div>
																																																																																																																																																																																																				<div class="form-group required">
															<label class="control-label col-sm-12">Color</label>
															<div class="col-sm-12">
																<div id="input-option4005007">
																																		<div class="radio">
																		<label for="option[409005007]">
																			<input type="radio" hidden name="option[40]" id="option[409005007]" value="90"/>
																			black																																					</label>
																	</div>
																																		<div class="radio">
																		<label for="option[409205007]">
																			<input type="radio" hidden name="option[40]" id="option[409205007]" value="92"/>
																			white																																					</label>
																	</div>
																																		<div class="radio">
																		<label for="option[409105007]">
																			<input type="radio" hidden name="option[40]" id="option[409105007]" value="91"/>
																			red																																					</label>
																	</div>
																																	</div>
															</div>
														</div>
																																																																																																																																																										<button class="product-btn-add" type="button" onclick="cart.addPopup($(this),'50');">
															<i class="material-design-shopping231"></i>
															<span>Add to Cart</span>
														</button>
													</div>
												</div>
											</div>
																						<div class="quick_info">
												<div id="quickview_50070" class="quickview-style">
													<div>
														<div class="left col-sm-4">
															<div class="quickview_image image">
																<a href="https://livedemo00.template-help.com/opencart_59086/index.php?route=product/product&amp;product_id=50"><img width="5" height="5" alt="Youngman 15 Colors Professional Concealer" title="Youngman 15 Colors Professional Concealer" class="img-responsive" src="image/cache/catalog/products/product_58-5x5.png"/></a>
															</div>
														</div>
														<div class="right col-sm-8">
															<h2>Youngman 15 Colors Professional Concealer</h2>
															<div class="inf">
																																<p class="quickview_manufacture manufacture manufacture">Brand:																	<a href="https://livedemo00.template-help.com/opencart_59086/index.php?route=product/manufacturer/info&amp;manufacturer_id=34">Borghese</a>
																</p>
																																																<p class="product_model model">Model: Classic</p>
																																																<div class="price">
																																		$85.00																																																		</div>
																															</div>
																														<button class="product-btn-add" data-toggle="tooltip" title="Add to Cart" type="button" onclick="cart.add('50');">
																<i class="material-design-shopping231"></i>
																<span>Add to Cart</span>
															</button>
															<ul class="product-buttons">
																<li>
																	<button class="product-btn" type="button" data-toggle="tooltip" title="Add to Wishlist" onclick="wishlist.add('50');">
																		<i class="fa fa-heart"></i>
																		<span>Add to Wishlist</span>
																	</button>
																</li>
																<li>
																	<button class="product-btn" type="button" data-toggle="tooltip" title="Add to Compare" onclick="compare.add('50');">
																		<i class="fa fa-exchange"></i>
																		<span>Add to Compare</span>
																	</button>
																</li>
															</ul>
															<div class="clear"></div>
														</div>
														<div class="col-sm-12">
															<div class="quickview_description description">
																
<p>In the modern world, there are dozens of means that can help you achieve a desired look and establish a proper image. One of such means is cosmetics. They include a vast amount of tools designed to enhance your attractiveness. There are also some subcategories of cosmetics that offer specialized care for your skin or that can be used to highlight facial features (your eyes, brows, lips, cheeks etc.) As these products have been widely used since the very beginning of our civilization, today they are available in the greatest variety from lipsticks to face powders with different colors and shapes.</p>
<p>Due to an increasing amount of makeup-related products, we decided to offer our expert help to a lot of people looking for high-quality cosmetics. At our store, you can easily find any type of makeup, hair dye, and deodorants or skincare products to satisfy the most exquisite tastes and needs, both men’s and women’s. As an industry leader in providing top-notch makeup and cosmetic products at affordable price, we can assure you won’t be disappointed by making a purchase at our website.</p>
<p>We care about our customers and their buying experience. That’s why our team pays a lot of attention to your order’s delivery process and provides full support on the stage of choosing a product that would suit you and be an ideal addition to your style. Moreover, our store provides regular discounts to our loyal customers who want to be well informed about the latest trends in cosmetics or the way they can benefit from buying at our store. If you are looking for new makeup tools to fill up your bag or are just wondering what it takes to look beautiful, visit our website and we will be glad to help you!</p>															</div>
														</div>
													</div>
												</div>
											</div>
																																	<div class="new-pr"><span>New!</span></div>
																						<div class="image">
												<a class="lazy" style="padding-bottom: 100%" href="https://livedemo00.template-help.com/opencart_59086/index.php?route=product/product&amp;product_id=50">
													<img width="5" height="5" alt="Youngman 15 Colors Professional Concealer" title="Youngman 15 Colors Professional Concealer" class="img-responsive" data-src="image/cache/catalog/products/product_58-5x5.png" src="#"/>
												</a>
												<a class="quickview" data-rel="details" href="#quickview_50070">
													Quick View												</a>
																							</div>
											<div class="caption">
												<div class="name name__aside">
													<a href="https://livedemo00.template-help.com/opencart_59086/index.php?route=product/product&amp;product_id=50">Youngman 15 Colors Professional Concealer</a>
												</div>
																								<div class="price">
																										$85.00																																						</div>
																								<div class="name">
													<a href="https://livedemo00.template-help.com/opencart_59086/index.php?route=product/product&amp;product_id=50">Youngman 15 Colors Professional Concealer</a>
												</div>												
											</div>
											<div class="cart-button">
												<button class="btn-primary" type="button"  onclick="ajaxAdd($(this),50);" >
													<i class="fl-outicons-shopping-cart13"></i> <span>Add to Cart</span>
												</button>
												<div class="clear"></div>
												<button class="product-btn" type="button" onclick="wishlist.add('50');">
													<i class="material-icons-favorite_border"></i> <span>Add to Wishlist</span>
												</button>
												<button class="product-btn" type="button" onclick="compare.add('50');">
													<i class="material-icons-shuffle"></i> <span>Add to Compare</span>
												</button>
											</div>
										</div>
																			</div>
																										</div>
							</div>
						</div>
																														<div class="view-all">
							<a href='https://livedemo00.template-help.com/opencart_59086/index.php?route=product/category&amp;path=18'>View All Products</a>
						</div>
					</div>
				</div>
			</div>
		</div>

						</div>
																							</div>
														<div class="sf-mega_section 0" style="width: 32.5%">
																		<div class="megamenu_module">
							<script>
				$(document).ready(function ($) {
				$('#single-category1 .tab-content > .tab-pane').css({
					'display': 'block',
					'visibility': 'visible'
				});
			});
					</script>

		<div class="box single-category">
			<div class="box-heading">
				<h2>Face</h2>
			</div>
			<div class="box-content">
				<div role="tabpanel" class="module_tab" id="single-category1">
																				<h3>New Arrivals</h3>
																				
					<!-- Tab panes -->
					<div class="tab-content">
																		<div role="tabpanel" class="tab-pane" id="tab-single-latest-1">
							<div class="box clearfix">
								<div class="row">
																											<div class="product-layout col-lg-3 col-md-3 col-sm-3 col-xs-12">
																				<div class="product-thumb transition options">
																						<!-- Product options -->
											<div class="product-option-wrap">
												<div class="product-options form-horizontal">
													<div class="options">
														<a class="ajax-overlay_close" href='#'></a>
														<h3>Available Options</h3>
														<div class="form-group hidden">
															<div class="col-sm-8">
																<input type="text" name="product_id" value="50" class="form-control"/>
															</div>
														</div>
																																										<div class="form-group required">
															<label class="control-label col-sm-12" for="input-option3916001">
																Size															</label>
															<div class="col-sm-12">
																<select name="option[39]" id="input-option3916001" class="form-control">
																	<option value="">--- Please Select ---</option>
																																		<option value="87">
																		Small																																				(+$15.00)
																																			</option>
																																		<option value="88">
																		Medium																																				(+$10.00)
																																			</option>
																																		<option value="89">
																		Large																																				(+$5.00)
																																			</option>
																																	</select>
															</div>
														</div>
																																																																																																																																																																																																				<div class="form-group required">
															<label class="control-label col-sm-12">Color</label>
															<div class="col-sm-12">
																<div id="input-option4016001">
																																		<div class="radio">
																		<label for="option[409016001]">
																			<input type="radio" hidden name="option[40]" id="option[409016001]" value="90"/>
																			black																																					</label>
																	</div>
																																		<div class="radio">
																		<label for="option[409216001]">
																			<input type="radio" hidden name="option[40]" id="option[409216001]" value="92"/>
																			white																																					</label>
																	</div>
																																		<div class="radio">
																		<label for="option[409116001]">
																			<input type="radio" hidden name="option[40]" id="option[409116001]" value="91"/>
																			red																																					</label>
																	</div>
																																	</div>
															</div>
														</div>
																																																																																																																																																										<button class="product-btn-add" type="button" onclick="cart.addPopup($(this),'50');">
															<i class="material-design-shopping231"></i>
															<span>Add to Cart</span>
														</button>
													</div>
												</div>
											</div>
																						<div class="quick_info">
												<div id="quickview_latest_60011" class="quickview-style">
													<div>
														<div class="left col-sm-4">
															<div class="quickview_image image">
																<a href="https://livedemo00.template-help.com/opencart_59086/index.php?route=product/product&amp;product_id=50"><img width="140" height="140" alt="Youngman 15 Colors Professional Concealer" title="Youngman 15 Colors Professional Concealer" class="img-responsive" src="image/cache/catalog/products/product_58-140x140.png"/></a>
															</div>
														</div>
														<div class="right col-sm-8">
															<h2>Youngman 15 Colors Professional Concealer</h2>
															<div class="inf">
																																<p class="quickview_manufacture manufacture manufacture">Brand:																	<a href="https://livedemo00.template-help.com/opencart_59086/index.php?route=product/manufacturer/info&amp;manufacturer_id=34">Borghese</a>
																</p>
																																																<p class="product_model model">Model: Classic</p>
																																																<div class="price">
																																		$85.00																																																		</div>
																															</div>
																														<button class="product-btn-add" data-toggle="tooltip" title="Add to Cart" type="button" onclick="cart.add('50');">
																<i class="material-design-shopping231"></i>
																<span>Add to Cart</span>
															</button>
															<ul class="product-buttons">
																<li>
																	<button class="product-btn" type="button" data-toggle="tooltip" title="Add to Wishlist" onclick="wishlist.add('50');">
																		<i class="fa fa-heart"></i>
																		<span>Add to Wishlist</span>
																	</button>
																</li>
																<li>
																	<button class="product-btn" type="button" data-toggle="tooltip" title="Add to Compare" onclick="compare.add('50');">
																		<i class="fa fa-exchange"></i>
																		<span>Add to Compare</span>
																	</button>
																</li>
															</ul>
															<div class="clear"></div>
														</div>
														<div class="col-sm-12">
															<div class="quickview_description description">
																
<p>In the modern world, there are dozens of means that can help you achieve a desired look and establish a proper image. One of such means is cosmetics. They include a vast amount of tools designed to enhance your attractiveness. There are also some subcategories of cosmetics that offer specialized care for your skin or that can be used to highlight facial features (your eyes, brows, lips, cheeks etc.) As these products have been widely used since the very beginning of our civilization, today they are available in the greatest variety from lipsticks to face powders with different colors and shapes.</p>
<p>Due to an increasing amount of makeup-related products, we decided to offer our expert help to a lot of people looking for high-quality cosmetics. At our store, you can easily find any type of makeup, hair dye, and deodorants or skincare products to satisfy the most exquisite tastes and needs, both men’s and women’s. As an industry leader in providing top-notch makeup and cosmetic products at affordable price, we can assure you won’t be disappointed by making a purchase at our website.</p>
<p>We care about our customers and their buying experience. That’s why our team pays a lot of attention to your order’s delivery process and provides full support on the stage of choosing a product that would suit you and be an ideal addition to your style. Moreover, our store provides regular discounts to our loyal customers who want to be well informed about the latest trends in cosmetics or the way they can benefit from buying at our store. If you are looking for new makeup tools to fill up your bag or are just wondering what it takes to look beautiful, visit our website and we will be glad to help you!</p>															</div>
														</div>
													</div>
												</div>
											</div>
																																	<div class="new-pr"><span>New!</span></div>
																						<div class="image">
												<a class="lazy" style="padding-bottom: 100%" href="https://livedemo00.template-help.com/opencart_59086/index.php?route=product/product&amp;product_id=50">
													<img width="140" height="140" alt="Youngman 15 Colors Professional Concealer" title="Youngman 15 Colors Professional Concealer" class="img-responsive" data-src="image/cache/catalog/products/product_58-140x140.png" src="#"/>
												</a>
												<a class="quickview" data-rel="details" href="#quickview_latest_60011">
													Quick View												</a>
																							</div>
											<div class="caption">
												<div class="name name__aside">
													<a href="https://livedemo00.template-help.com/opencart_59086/index.php?route=product/product&amp;product_id=50">Youngman 15 Colors Professional Concealer</a>
												</div>
																								<div class="price">
																										$85.00																																						</div>
																								<div class="name">
													<a href="https://livedemo00.template-help.com/opencart_59086/index.php?route=product/product&amp;product_id=50">Youngman 15 Colors Professional Concealer</a>
												</div>												
											</div>
											<div class="cart-button">
												<button class="btn-primary" type="button"  onclick="ajaxAdd($(this),50);" >
													<i class="fl-outicons-shopping-cart13"></i> <span>Add to Cart</span>
												</button>
												<div class="clear"></div>
												<button class="product-btn" type="button" onclick="wishlist.add('50');">
													<i class="material-icons-favorite_border"></i> <span>Add to Wishlist</span>
												</button>
												<button class="product-btn" type="button" onclick="compare.add('50');">
													<i class="material-icons-shuffle"></i> <span>Add to Compare</span>
												</button>
											</div>
										</div>
																			</div>
																																				<div class="product-layout col-lg-3 col-md-3 col-sm-3 col-xs-12">
																				<div class="product-thumb transition options">
																						<!-- Product options -->
											<div class="product-option-wrap">
												<div class="product-options form-horizontal">
													<div class="options">
														<a class="ajax-overlay_close" href='#'></a>
														<h3>Available Options</h3>
														<div class="form-group hidden">
															<div class="col-sm-8">
																<input type="text" name="product_id" value="49" class="form-control"/>
															</div>
														</div>
																																										<div class="form-group required">
															<label class="control-label col-sm-12" for="input-option3716002">
																Size															</label>
															<div class="col-sm-12">
																<select name="option[37]" id="input-option3716002" class="form-control">
																	<option value="">--- Please Select ---</option>
																																		<option value="83">
																		Small																																				(+$20.00)
																																			</option>
																																		<option value="84">
																		Medium																																				(+$15.00)
																																			</option>
																																		<option value="85">
																		Large																																				(+$10.00)
																																			</option>
																																	</select>
															</div>
														</div>
																																																																																																																																																																																																				<div class="form-group required">
															<label class="control-label col-sm-12">Color</label>
															<div class="col-sm-12">
																<div id="input-option3816002">
																																		<div class="radio">
																		<label for="option[388616002]">
																			<input type="radio" hidden name="option[38]" id="option[388616002]" value="86"/>
																			red																																						(+$10.00)
																																					</label>
																	</div>
																																	</div>
															</div>
														</div>
																																																																																																																																																										<button class="product-btn-add" type="button" onclick="cart.addPopup($(this),'49');">
															<i class="material-design-shopping231"></i>
															<span>Add to Cart</span>
														</button>
													</div>
												</div>
											</div>
																						<div class="quick_info">
												<div id="quickview_latest_60021" class="quickview-style">
													<div>
														<div class="left col-sm-4">
															<div class="quickview_image image">
																<a href="https://livedemo00.template-help.com/opencart_59086/index.php?route=product/product&amp;product_id=49"><img width="140" height="140" alt="VERAttiva Face Tonic Rinse and Refresher 7-Ounce" title="VERAttiva Face Tonic Rinse and Refresher 7-Ounce" class="img-responsive" src="image/cache/catalog/products/product_55-140x140.png"/></a>
															</div>
														</div>
														<div class="right col-sm-8">
															<h2>VERAttiva Face Tonic Rinse and Refresher 7-Ounce</h2>
															<div class="inf">
																																<p class="quickview_manufacture manufacture manufacture">Brand:																	<a href="https://livedemo00.template-help.com/opencart_59086/index.php?route=product/manufacturer/info&amp;manufacturer_id=33">Clarins</a>
																</p>
																																																<p class="product_model model">Model: Deluxe</p>
																																																<div class="price">
																																		<span class="price-new">$56.00</span>
																	<span class="price-old">$70.00</span>
																																																		</div>
																															</div>
																														<div class="rating">
																																																<span class="fa-stack">
																	<i class="material-design-bookmark45 fa-stack-1x"></i>
																	<i class="material-design-bookmark45 star fa-stack-1x"></i>
																</span>
																																																																<span class="fa-stack">
																	<i class="material-design-bookmark45 fa-stack-1x"></i>
																	<i class="material-design-bookmark45 star fa-stack-1x"></i>
																</span>
																																																																<span class="fa-stack">
																	<i class="material-design-bookmark45 fa-stack-1x"></i>
																	<i class="material-design-bookmark45 star fa-stack-1x"></i>
																</span>
																																																																<span class="fa-stack">
																	<i class="material-design-bookmark45 fa-stack-1x"></i>
																	<i class="material-design-bookmark45 star fa-stack-1x"></i>
																</span>
																																																																<span class="fa-stack">
																	<i class="material-design-bookmark45 fa-stack-1x"></i>
																	<i class="material-design-bookmark45 star fa-stack-1x"></i>
																</span>
																																															</div>
																														<button class="product-btn-add" data-toggle="tooltip" title="Add to Cart" type="button" onclick="cart.add('49');">
																<i class="material-design-shopping231"></i>
																<span>Add to Cart</span>
															</button>
															<ul class="product-buttons">
																<li>
																	<button class="product-btn" type="button" data-toggle="tooltip" title="Add to Wishlist" onclick="wishlist.add('49');">
																		<i class="fa fa-heart"></i>
																		<span>Add to Wishlist</span>
																	</button>
																</li>
																<li>
																	<button class="product-btn" type="button" data-toggle="tooltip" title="Add to Compare" onclick="compare.add('49');">
																		<i class="fa fa-exchange"></i>
																		<span>Add to Compare</span>
																	</button>
																</li>
															</ul>
															<div class="clear"></div>
														</div>
														<div class="col-sm-12">
															<div class="quickview_description description">
																
<p>In the modern world, there are dozens of means that can help you achieve a desired look and establish a proper image. One of such means is cosmetics. They include a vast amount of tools designed to enhance your attractiveness. There are also some subcategories of cosmetics that offer specialized care for your skin or that can be used to highlight facial features (your eyes, brows, lips, cheeks etc.) As these products have been widely used since the very beginning of our civilization, today they are available in the greatest variety from lipsticks to face powders with different colors and shapes.</p>
<p>Due to an increasing amount of makeup-related products, we decided to offer our expert help to a lot of people looking for high-quality cosmetics. At our store, you can easily find any type of makeup, hair dye, and deodorants or skincare products to satisfy the most exquisite tastes and needs, both men’s and women’s. As an industry leader in providing top-notch makeup and cosmetic products at affordable price, we can assure you won’t be disappointed by making a purchase at our website.</p>
<p>We care about our customers and their buying experience. That’s why our team pays a lot of attention to your order’s delivery process and provides full support on the stage of choosing a product that would suit you and be an ideal addition to your style. Moreover, our store provides regular discounts to our loyal customers who want to be well informed about the latest trends in cosmetics or the way they can benefit from buying at our store. If you are looking for new makeup tools to fill up your bag or are just wondering what it takes to look beautiful, visit our website and we will be glad to help you!</p>															</div>
														</div>
													</div>
												</div>
											</div>
																																	<div class="sale">
												<span>Sale!</span>
											</div>
																																																							<div class="image">
												<a class="lazy" style="padding-bottom: 100%" href="https://livedemo00.template-help.com/opencart_59086/index.php?route=product/product&amp;product_id=49">
													<img width="140" height="140" alt="VERAttiva Face Tonic Rinse and Refresher 7-Ounce" title="VERAttiva Face Tonic Rinse and Refresher 7-Ounce" class="img-responsive" data-src="image/cache/catalog/products/product_55-140x140.png" src="#"/>
												</a>
												<a class="quickview" data-rel="details" href="#quickview_latest_60021">
													Quick View												</a>
																									<div class="rating">
																																										<span class="fa-stack"> <i class="fa fa-star active fa-stack-1x"></i> </span>
																																																								<span class="fa-stack"> <i class="fa fa-star active fa-stack-1x"></i> </span>
																																																								<span class="fa-stack"> <i class="fa fa-star active fa-stack-1x"></i> </span>
																																																								<span class="fa-stack"> <i class="fa fa-star active fa-stack-1x"></i> </span>
																																																								<span class="fa-stack"> <i class="fa fa-star active fa-stack-1x"></i> </span>
																																									</div>
																							</div>
											<div class="caption">
												<div class="name name__aside">
													<a href="https://livedemo00.template-help.com/opencart_59086/index.php?route=product/product&amp;product_id=49">VERAttiva Face Tonic Rinse and Refresher 7-Ounce</a>
												</div>
																								<div class="price">
																										<span class="price-new">$56.00</span>
													<span class="price-old">$70.00</span>
																																						</div>
																								<div class="name">
													<a href="https://livedemo00.template-help.com/opencart_59086/index.php?route=product/product&amp;product_id=49">VERAttiva Face Tonic Rinse and Refresher 7-Ounce</a>
												</div>												
											</div>
											<div class="cart-button">
												<button class="btn-primary" type="button"  onclick="ajaxAdd($(this),49);" >
													<i class="fl-outicons-shopping-cart13"></i> <span>Add to Cart</span>
												</button>
												<div class="clear"></div>
												<button class="product-btn" type="button" onclick="wishlist.add('49');">
													<i class="material-icons-favorite_border"></i> <span>Add to Wishlist</span>
												</button>
												<button class="product-btn" type="button" onclick="compare.add('49');">
													<i class="material-icons-shuffle"></i> <span>Add to Compare</span>
												</button>
											</div>
										</div>
																			</div>
																										</div>
							</div>
						</div>
																								<div class="view-all">
							<a href='https://livedemo00.template-help.com/opencart_59086/index.php?route=product/category&amp;path=18'>View All Products</a>
						</div>
					</div>
				</div>
			</div>
		</div>

						</div>
																							</div>
												</ul>
					</li>
				<li >
						<a href="https://livedemo00.template-help.com/opencart_59086/index.php?route=product/category&amp;path=20">Body</a>
									
					</li>
				<li >
						<a href="https://livedemo00.template-help.com/opencart_59086/index.php?route=product/category&amp;path=25">Make up</a>
									
					</li>
				<li >
						<a href="https://livedemo00.template-help.com/opencart_59086/index.php?route=product/category&amp;path=28">Hair</a>
									
					</li>
				<li >
						<a href="https://livedemo00.template-help.com/opencart_59086/index.php?route=product/category&amp;path=29">Perfumes</a>
									
					</li>
				<li >
						<a href="https://livedemo00.template-help.com/opencart_59086/index.php?route=product/category&amp;path=30">Must haves</a>
									
					</li>
				<li >
						<a href="index.php?route=product/manufacturer">Brands</a>
									
					</li>
				<li >
						<a href="index.php?route=account/voucher">Gifts</a>
									
					</li>
			</ul>
</div>
<script>
	;(function ($) {
		$(window).load(function () {
			var o = $('.sf-menu');
			o.superfish();
			o.find('li a').each(function () {
				if ($(location).attr('href').indexOf($(this).attr('href')) >= 0){
					$(this).addClass('active');
					return;
				}
			})
			if (o.parents('aside').length){
				var width = $('.container').outerWidth() - $('aside').outerWidth();
				o.find('.sf-mega').each(function () {
					$(this).width(width);
				})
			}
		});
	})(jQuery);
</script>
<div id="style_switcher">
	<div class="toggler"></div>
	<p>The customization tool allows you to make color changes in your theme</p>
	<ul id="color-box">
						<li><div class="color_scheme color_scheme_1" data-scheme="color_scheme_1">&nbsp;</div></li>
								<li><div class="color_scheme color_scheme_2" data-scheme="color_scheme_2">&nbsp;</div></li>
								<li><div class="color_scheme color_scheme_3" data-scheme="color_scheme_3">&nbsp;</div></li>
								<li class="active"><div class="color_scheme color_scheme_4" data-scheme="color_scheme_4">&nbsp;</div></li>
					</ul>
</div>
					</div>				
								<div class="search">
	<a class="top-search"><i class="fl-outicons-magnifying-glass34"></i></a>
	<div id="search">
		<div class="inner">
		<input type="text" name="search" value="" placeholder="Search..." /><button type="button" class="button-search"><i class="fl-outicons-magnifying-glass34"></i></button>
		</div>
		<div class="clear"></div>
	</div>
</div>			</div></div>
					</header><div class="container">
	<div class="row">								<div id="content" class="col-sm-12"><div id="banner0" class="banners">
			<div class="col-sm-12 banner-1">
		<div class="banner-box">
			<a class="clearfix" href="index.php?route=product/product&amp;product_id=50">
				<img src="image/cache/catalog/banner-1-1065x660.jpg" alt="banner-1" class="img-responsive" />
				<div class="over"></div>
								<div class="s-desc"><h2><span>Lipsticks</span><br>
Improve your look with new MaxFactor Colour Elixir Lipstick</h2>
<p>Add some charm to your image with new series of MaxFactor Lipstick available to order at our store.</p>
<span class="banner_price">$25.99</span></div>
							</a>
		</div>
	</div>
		</div>

<div id="banner1" class="banners">
			<div class="col-sm-12 banner-2">
		<div class="banner-box">
			<a class="clearfix" href="index.php?route=product/product&amp;product_id=49">
				<img src="image/cache/catalog/banner-2-525x322.jpg" alt="banner-2" class="img-responsive" />
				<div class="over"></div>
								<div class="s-desc"><h3><span>Powders</span><br>
Get your favorite powder at a half price</h3></div>
							</a>
		</div>
	</div>
				<div class="col-sm-12 banner-3">
		<div class="banner-box">
			<a class="clearfix" href="index.php?route=product/product&amp;product_id=48">
				<img src="image/cache/catalog/banner-3-525x322.jpg" alt="banner-3" class="img-responsive" />
				<div class="over"></div>
								<div class="s-desc"><h3><span>Perfumes</span><br>
New Calvin Klein Eau de Parfum for her</h3></div>
							</a>
		</div>
	</div>
		</div>

<div class="box latest">
<div class="clear"></div>
	<div class="box-heading">
		<h3>New Products</h3>
	</div>
	<div class="box-content">
		<div class="box-carousel">
										<div class="product-thumb transition options">
										<!-- Product options -->
					<div class="product-option-wrap">
						<div class="product-options form-horizontal">
							<div class="options">
								<a class="ajax-overlay_close" href='#'></a>
								<h3>Available Options</h3>
								<div class="form-group hidden">
									<div class="col-sm-8">
										<input type="text" name="product_id" value="50" class="form-control"/>
									</div>
								</div>
																								<div class="form-group required">
									<label class="control-label col-sm-12" for="input-option3903001">
										Size									</label>
									<div class="col-sm-12">
										<select name="option[39]" id="input-option3903001" class="form-control">
											<option value="">--- Please Select ---</option>
																						<option value="87">
												Small												(+$15.00)&lrm;											</option>
																						<option value="88">
												Medium												(+$10.00)&lrm;											</option>
																						<option value="89">
												Large												(+$5.00)&lrm;											</option>
																					</select>
									</div>
								</div>
																																																																																																																<div class="form-group required">
									<label class="control-label col-sm-12">
										Color									</label>
									<div class="col-sm-12">
										<div id="input-option4003001">
																						<div class="radio">
												<label for="option[409003001]">
													<input type="radio" hidden name="option[40]" id="option[409003001]" value="90"/>
													black																									</label>
											</div>
																						<div class="radio">
												<label for="option[409203001]">
													<input type="radio" hidden name="option[40]" id="option[409203001]" value="92"/>
													white																									</label>
											</div>
																						<div class="radio">
												<label for="option[409103001]">
													<input type="radio" hidden name="option[40]" id="option[409103001]" value="91"/>
													red																									</label>
											</div>
																					</div>
									</div>
								</div>
																																																																																								<button class="btn-primary" type="button" onclick="cart.addPopup($(this),'50');">
									<i class="fl-outicons-shopping-cart13"></i>  <span>Add to Cart</span>
								</button>
							</div>
						</div>
					</div>
										<div class="quick_info">
						<div id="quickview_latest_30010" class="quickview-style">
							<div class="left col-sm-5">
								<div class="quickview_image image">
									<a href="https://livedemo00.template-help.com/opencart_59086/index.php?route=product/product&amp;product_id=50">
										<img width="310" height="310" alt="Youngman 15 Colors Professional Concealer" title="Youngman 15 Colors Professional Concealer" class="img-responsive" src="image/cache/catalog/products/product_58-310x310.png"/>
									</a>
								</div>
							</div>
							<div class="right col-sm-7">
								<h2>Youngman 15 Colors Professional Concealer</h2>								
																<div class="inf">
																		<p class="quickview_manufacture manufacture manufacture">
										Brand:										<a href="https://livedemo00.template-help.com/opencart_59086/index.php?route=product/manufacturer/info&amp;manufacturer_id=34">Borghese</a>
									</p>
																											<p class="product_model model">
										Model: Classic									</p>
																		
								</div>								
																	<div class="price">
																				$85.00																													</div>
																<button class="btn-primary" data-toggle="tooltip" title="Add to Cart" type="button" onclick="cart.add('50');"> <i class="fl-outicons-shopping-cart13"></i> <span>Add to Cart</span> </button>
								<ul class="product-buttons">
									<li>
										<button class="product-btn" type="button" data-toggle="tooltip" title="Add to Wishlist" onclick="wishlist.add('50');">
											<i class="material-icons-favorite_border"></i> <span>Add to Wishlist</span>
										</button>
									</li>
									<li>
										<button class="product-btn" type="button" data-toggle="tooltip" title="Add to Compare" onclick="compare.add('50');">
											<i class="material-icons-shuffle"></i> <span>Add to Compare</span>
										</button>
									</li>
								</ul>
								<div class="clear"></div>
							</div>
						</div>
					</div>
															<div class="new-pr"><span>New</span></div>
										<div class="image">
						<a class="lazy" style="padding-bottom: 100%" href="https://livedemo00.template-help.com/opencart_59086/index.php?route=product/product&amp;product_id=50">
							<img width="310" height="310" alt="Youngman 15 Colors Professional Concealer" title="Youngman 15 Colors Professional Concealer" class="img-responsive" data-src="image/cache/catalog/products/product_58-310x310.png" src="#"/>
						</a>
						<a class="quickview" data-rel="details" href="#quickview_latest_30010">
							Quick View						</a>
											</div>
					<div class="caption">						
						<div class="name">
							<a href="https://livedemo00.template-help.com/opencart_59086/index.php?route=product/product&amp;product_id=50">Youngman 15 Colors Professional Concealer</a>
						</div>
						<div class="category-name">
							Face 
						</div>						
												<div class="price">
														$85.00																				</div>
											</div>
					<div class="cart-button">
						<button class="btn-primary" type="button"  onclick="ajaxAdd($(this),50);" > <i class="fl-outicons-shopping-cart13"></i> <span>Add to Cart</span> </button>
						<div class="clear"></div>
						<button class="product-btn" type="button" onclick="wishlist.add('50');"> <i class="material-icons-favorite_border"></i> <span>Add to Wishlist</span></button>
						<button class="product-btn" type="button" onclick="compare.add('50');"> <i class="material-icons-shuffle"></i> <span>Add to Compare</span></button>
					</div>
				</div>
														<div class="product-thumb transition options">
										<!-- Product options -->
					<div class="product-option-wrap">
						<div class="product-options form-horizontal">
							<div class="options">
								<a class="ajax-overlay_close" href='#'></a>
								<h3>Available Options</h3>
								<div class="form-group hidden">
									<div class="col-sm-8">
										<input type="text" name="product_id" value="49" class="form-control"/>
									</div>
								</div>
																								<div class="form-group required">
									<label class="control-label col-sm-12" for="input-option3703002">
										Size									</label>
									<div class="col-sm-12">
										<select name="option[37]" id="input-option3703002" class="form-control">
											<option value="">--- Please Select ---</option>
																						<option value="83">
												Small												(+$20.00)&lrm;											</option>
																						<option value="84">
												Medium												(+$15.00)&lrm;											</option>
																						<option value="85">
												Large												(+$10.00)&lrm;											</option>
																					</select>
									</div>
								</div>
																																																																																																																<div class="form-group required">
									<label class="control-label col-sm-12">
										Color									</label>
									<div class="col-sm-12">
										<div id="input-option3803002">
																						<div class="radio">
												<label for="option[388603002]">
													<input type="radio" hidden name="option[38]" id="option[388603002]" value="86"/>
													red													(+$10.00)&lrm;												</label>
											</div>
																					</div>
									</div>
								</div>
																																																																																								<button class="btn-primary" type="button" onclick="cart.addPopup($(this),'49');">
									<i class="fl-outicons-shopping-cart13"></i>  <span>Add to Cart</span>
								</button>
							</div>
						</div>
					</div>
										<div class="quick_info">
						<div id="quickview_latest_30020" class="quickview-style">
							<div class="left col-sm-5">
								<div class="quickview_image image">
									<a href="https://livedemo00.template-help.com/opencart_59086/index.php?route=product/product&amp;product_id=49">
										<img width="310" height="310" alt="VERAttiva Face Tonic Rinse and Refresher 7-Ounce" title="VERAttiva Face Tonic Rinse and Refresher 7-Ounce" class="img-responsive" src="image/cache/catalog/products/product_55-310x310.png"/>
									</a>
								</div>
							</div>
							<div class="right col-sm-7">
								<h2>VERAttiva Face Tonic Rinse and Refresher 7-Ounce</h2>								
																<div class="rating">
																											<span class="fa-stack"> <i class="fa fa-star active fa-stack-1x"></i> </span>
																																				<span class="fa-stack"> <i class="fa fa-star active fa-stack-1x"></i> </span>
																																				<span class="fa-stack"> <i class="fa fa-star active fa-stack-1x"></i> </span>
																																				<span class="fa-stack"> <i class="fa fa-star active fa-stack-1x"></i> </span>
																																				<span class="fa-stack"> <i class="fa fa-star active fa-stack-1x"></i> </span>
																										</div>
																<div class="inf">
																		<p class="quickview_manufacture manufacture manufacture">
										Brand:										<a href="https://livedemo00.template-help.com/opencart_59086/index.php?route=product/manufacturer/info&amp;manufacturer_id=33">Clarins</a>
									</p>
																											<p class="product_model model">
										Model: Deluxe									</p>
																		
								</div>								
																	<div class="price">
																				<span class="price-new">$56.00</span>
																													</div>
																<button class="btn-primary" data-toggle="tooltip" title="Add to Cart" type="button" onclick="cart.add('49');"> <i class="fl-outicons-shopping-cart13"></i> <span>Add to Cart</span> </button>
								<ul class="product-buttons">
									<li>
										<button class="product-btn" type="button" data-toggle="tooltip" title="Add to Wishlist" onclick="wishlist.add('49');">
											<i class="material-icons-favorite_border"></i> <span>Add to Wishlist</span>
										</button>
									</li>
									<li>
										<button class="product-btn" type="button" data-toggle="tooltip" title="Add to Compare" onclick="compare.add('49');">
											<i class="material-icons-shuffle"></i> <span>Add to Compare</span>
										</button>
									</li>
								</ul>
								<div class="clear"></div>
							</div>
						</div>
					</div>
																		<div class="sale">
								<span>Sale</span>
							</div>
																											<div class="image">
						<a class="lazy" style="padding-bottom: 100%" href="https://livedemo00.template-help.com/opencart_59086/index.php?route=product/product&amp;product_id=49">
							<img width="310" height="310" alt="VERAttiva Face Tonic Rinse and Refresher 7-Ounce" title="VERAttiva Face Tonic Rinse and Refresher 7-Ounce" class="img-responsive" data-src="image/cache/catalog/products/product_55-310x310.png" src="#"/>
						</a>
						<a class="quickview" data-rel="details" href="#quickview_latest_30020">
							Quick View						</a>
													<div class="rating">
																								<span class="fa-stack"> <i class="fa fa-star active fa-stack-1x"></i> </span>
																																<span class="fa-stack"> <i class="fa fa-star active fa-stack-1x"></i> </span>
																																<span class="fa-stack"> <i class="fa fa-star active fa-stack-1x"></i> </span>
																																<span class="fa-stack"> <i class="fa fa-star active fa-stack-1x"></i> </span>
																																<span class="fa-stack"> <i class="fa fa-star active fa-stack-1x"></i> </span>
																							</div>
											</div>
					<div class="caption">						
						<div class="name">
							<a href="https://livedemo00.template-help.com/opencart_59086/index.php?route=product/product&amp;product_id=49">VERAttiva Face Tonic Rinse and Refresher 7-Ounce</a>
						</div>
						<div class="category-name">
							Face 
						</div>						
												<div class="price">
														<span class="price-new">$56.00</span> 
							<span class="price-old">$70.00</span>
																				</div>
											</div>
					<div class="cart-button">
						<button class="btn-primary" type="button"  onclick="ajaxAdd($(this),49);" > <i class="fl-outicons-shopping-cart13"></i> <span>Add to Cart</span> </button>
						<div class="clear"></div>
						<button class="product-btn" type="button" onclick="wishlist.add('49');"> <i class="material-icons-favorite_border"></i> <span>Add to Wishlist</span></button>
						<button class="product-btn" type="button" onclick="compare.add('49');"> <i class="material-icons-shuffle"></i> <span>Add to Compare</span></button>
					</div>
				</div>
														<div class="product-thumb transition options">
										<!-- Product options -->
					<div class="product-option-wrap">
						<div class="product-options form-horizontal">
							<div class="options">
								<a class="ajax-overlay_close" href='#'></a>
								<h3>Available Options</h3>
								<div class="form-group hidden">
									<div class="col-sm-8">
										<input type="text" name="product_id" value="48" class="form-control"/>
									</div>
								</div>
																								<div class="form-group required">
									<label class="control-label col-sm-12" for="input-option3503003">
										Size									</label>
									<div class="col-sm-12">
										<select name="option[35]" id="input-option3503003" class="form-control">
											<option value="">--- Please Select ---</option>
																						<option value="79">
												Small												(+$20.00)&lrm;											</option>
																						<option value="80">
												Medium												(+$15.00)&lrm;											</option>
																						<option value="81">
												Large												(+$10.00)&lrm;											</option>
																					</select>
									</div>
								</div>
																																																																																																																<div class="form-group required">
									<label class="control-label col-sm-12">
										Color									</label>
									<div class="col-sm-12">
										<div id="input-option3603003">
																						<div class="radio">
												<label for="option[368203003]">
													<input type="radio" hidden name="option[36]" id="option[368203003]" value="82"/>
													red													(+$10.00)&lrm;												</label>
											</div>
																					</div>
									</div>
								</div>
																																																																																								<button class="btn-primary" type="button" onclick="cart.addPopup($(this),'48');">
									<i class="fl-outicons-shopping-cart13"></i>  <span>Add to Cart</span>
								</button>
							</div>
						</div>
					</div>
										<div class="quick_info">
						<div id="quickview_latest_30030" class="quickview-style">
							<div class="left col-sm-5">
								<div class="quickview_image image">
									<a href="https://livedemo00.template-help.com/opencart_59086/index.php?route=product/product&amp;product_id=48">
										<img width="310" height="310" alt="Eye and Lip Primer Base  Paraben Talc Free" title="Eye and Lip Primer Base  Paraben Talc Free" class="img-responsive" src="image/cache/catalog/products/product_52-310x310.png"/>
									</a>
								</div>
							</div>
							<div class="right col-sm-7">
								<h2>Eye and Lip Primer Base  Paraben Talc Free</h2>								
																<div class="rating">
																											<span class="fa-stack"> <i class="fa fa-star active fa-stack-1x"></i> </span>
																																				<span class="fa-stack"> <i class="fa fa-star active fa-stack-1x"></i> </span>
																																				<span class="fa-stack"> <i class="fa fa-star active fa-stack-1x"></i> </span>
																																				<span class="fa-stack"> <i class="fa fa-star active fa-stack-1x"></i> </span>
																																				<span class="fa-stack"> <i class="fa fa-star active fa-stack-1x"></i> </span>
																										</div>
																<div class="inf">
																		<p class="quickview_manufacture manufacture manufacture">
										Brand:										<a href="https://livedemo00.template-help.com/opencart_59086/index.php?route=product/manufacturer/info&amp;manufacturer_id=32">Verattiva</a>
									</p>
																											<p class="product_model model">
										Model: Collectible									</p>
																		
								</div>								
																	<div class="price">
																				$80.00																													</div>
																<button class="btn-primary" data-toggle="tooltip" title="Add to Cart" type="button" onclick="cart.add('48');"> <i class="fl-outicons-shopping-cart13"></i> <span>Add to Cart</span> </button>
								<ul class="product-buttons">
									<li>
										<button class="product-btn" type="button" data-toggle="tooltip" title="Add to Wishlist" onclick="wishlist.add('48');">
											<i class="material-icons-favorite_border"></i> <span>Add to Wishlist</span>
										</button>
									</li>
									<li>
										<button class="product-btn" type="button" data-toggle="tooltip" title="Add to Compare" onclick="compare.add('48');">
											<i class="material-icons-shuffle"></i> <span>Add to Compare</span>
										</button>
									</li>
								</ul>
								<div class="clear"></div>
							</div>
						</div>
					</div>
															<div class="new-pr"><span>New</span></div>
										<div class="image">
						<a class="lazy" style="padding-bottom: 100%" href="https://livedemo00.template-help.com/opencart_59086/index.php?route=product/product&amp;product_id=48">
							<img width="310" height="310" alt="Eye and Lip Primer Base  Paraben Talc Free" title="Eye and Lip Primer Base  Paraben Talc Free" class="img-responsive" data-src="image/cache/catalog/products/product_52-310x310.png" src="#"/>
						</a>
						<a class="quickview" data-rel="details" href="#quickview_latest_30030">
							Quick View						</a>
													<div class="rating">
																								<span class="fa-stack"> <i class="fa fa-star active fa-stack-1x"></i> </span>
																																<span class="fa-stack"> <i class="fa fa-star active fa-stack-1x"></i> </span>
																																<span class="fa-stack"> <i class="fa fa-star active fa-stack-1x"></i> </span>
																																<span class="fa-stack"> <i class="fa fa-star active fa-stack-1x"></i> </span>
																																<span class="fa-stack"> <i class="fa fa-star active fa-stack-1x"></i> </span>
																							</div>
											</div>
					<div class="caption">						
						<div class="name">
							<a href="https://livedemo00.template-help.com/opencart_59086/index.php?route=product/product&amp;product_id=48">Eye and Lip Primer Base  Paraben Talc Free</a>
						</div>
						<div class="category-name">
							Face 
						</div>						
												<div class="price">
														$80.00																				</div>
											</div>
					<div class="cart-button">
						<button class="btn-primary" type="button"  onclick="ajaxAdd($(this),48);" > <i class="fl-outicons-shopping-cart13"></i> <span>Add to Cart</span> </button>
						<div class="clear"></div>
						<button class="product-btn" type="button" onclick="wishlist.add('48');"> <i class="material-icons-favorite_border"></i> <span>Add to Wishlist</span></button>
						<button class="product-btn" type="button" onclick="compare.add('48');"> <i class="material-icons-shuffle"></i> <span>Add to Compare</span></button>
					</div>
				</div>
														<div class="product-thumb transition options">
										<!-- Product options -->
					<div class="product-option-wrap">
						<div class="product-options form-horizontal">
							<div class="options">
								<a class="ajax-overlay_close" href='#'></a>
								<h3>Available Options</h3>
								<div class="form-group hidden">
									<div class="col-sm-8">
										<input type="text" name="product_id" value="47" class="form-control"/>
									</div>
								</div>
																								<div class="form-group required">
									<label class="control-label col-sm-12" for="input-option3303004">
										Size									</label>
									<div class="col-sm-12">
										<select name="option[33]" id="input-option3303004" class="form-control">
											<option value="">--- Please Select ---</option>
																						<option value="75">
												Small												(+$20.00)&lrm;											</option>
																						<option value="76">
												Medium												(+$15.00)&lrm;											</option>
																						<option value="77">
												Large												(+$10.00)&lrm;											</option>
																					</select>
									</div>
								</div>
																																																																																																																<div class="form-group required">
									<label class="control-label col-sm-12">
										Color									</label>
									<div class="col-sm-12">
										<div id="input-option3403004">
																						<div class="radio">
												<label for="option[347803004]">
													<input type="radio" hidden name="option[34]" id="option[347803004]" value="78"/>
													red													(+$10.00)&lrm;												</label>
											</div>
																					</div>
									</div>
								</div>
																																																																																								<button class="btn-primary" type="button" onclick="cart.addPopup($(this),'47');">
									<i class="fl-outicons-shopping-cart13"></i>  <span>Add to Cart</span>
								</button>
							</div>
						</div>
					</div>
										<div class="quick_info">
						<div id="quickview_latest_30040" class="quickview-style">
							<div class="left col-sm-5">
								<div class="quickview_image image">
									<a href="https://livedemo00.template-help.com/opencart_59086/index.php?route=product/product&amp;product_id=47">
										<img width="310" height="310" alt="Physicians Formula Youthful Wear Cosmeceutical" title="Physicians Formula Youthful Wear Cosmeceutical" class="img-responsive" src="image/cache/catalog/products/product_49-310x310.png"/>
									</a>
								</div>
							</div>
							<div class="right col-sm-7">
								<h2>Physicians Formula Youthful Wear Cosmeceutical</h2>								
																<div class="rating">
																											<span class="fa-stack"> <i class="fa fa-star active fa-stack-1x"></i> </span>
																																				<span class="fa-stack"> <i class="fa fa-star active fa-stack-1x"></i> </span>
																																				<span class="fa-stack"> <i class="fa fa-star active fa-stack-1x"></i> </span>
																																				<span class="fa-stack"> <i class="fa fa-star active fa-stack-1x"></i> </span>
																																				<span class="fa-stack"> <i class="fa fa-star active fa-stack-1x"></i> </span>
																										</div>
																<div class="inf">
																		<p class="quickview_manufacture manufacture manufacture">
										Brand:										<a href="https://livedemo00.template-help.com/opencart_59086/index.php?route=product/manufacturer/info&amp;manufacturer_id=31">Shany</a>
									</p>
																											<p class="product_model model">
										Model: Premium									</p>
																		
								</div>								
																	<div class="price">
																				<span class="price-new">$48.00</span>
																													</div>
																<button class="btn-primary" data-toggle="tooltip" title="Add to Cart" type="button" onclick="cart.add('47');"> <i class="fl-outicons-shopping-cart13"></i> <span>Add to Cart</span> </button>
								<ul class="product-buttons">
									<li>
										<button class="product-btn" type="button" data-toggle="tooltip" title="Add to Wishlist" onclick="wishlist.add('47');">
											<i class="material-icons-favorite_border"></i> <span>Add to Wishlist</span>
										</button>
									</li>
									<li>
										<button class="product-btn" type="button" data-toggle="tooltip" title="Add to Compare" onclick="compare.add('47');">
											<i class="material-icons-shuffle"></i> <span>Add to Compare</span>
										</button>
									</li>
								</ul>
								<div class="clear"></div>
							</div>
						</div>
					</div>
																		<div class="sale">
								<span>Sale</span>
							</div>
																											<div class="image">
						<a class="lazy" style="padding-bottom: 100%" href="https://livedemo00.template-help.com/opencart_59086/index.php?route=product/product&amp;product_id=47">
							<img width="310" height="310" alt="Physicians Formula Youthful Wear Cosmeceutical" title="Physicians Formula Youthful Wear Cosmeceutical" class="img-responsive" data-src="image/cache/catalog/products/product_49-310x310.png" src="#"/>
						</a>
						<a class="quickview" data-rel="details" href="#quickview_latest_30040">
							Quick View						</a>
													<div class="rating">
																								<span class="fa-stack"> <i class="fa fa-star active fa-stack-1x"></i> </span>
																																<span class="fa-stack"> <i class="fa fa-star active fa-stack-1x"></i> </span>
																																<span class="fa-stack"> <i class="fa fa-star active fa-stack-1x"></i> </span>
																																<span class="fa-stack"> <i class="fa fa-star active fa-stack-1x"></i> </span>
																																<span class="fa-stack"> <i class="fa fa-star active fa-stack-1x"></i> </span>
																							</div>
											</div>
					<div class="caption">						
						<div class="name">
							<a href="https://livedemo00.template-help.com/opencart_59086/index.php?route=product/product&amp;product_id=47">Physicians Formula Youthful Wear Cosmeceutical</a>
						</div>
						<div class="category-name">
							Face 
						</div>						
												<div class="price">
														<span class="price-new">$48.00</span> 
							<span class="price-old">$60.00</span>
																				</div>
											</div>
					<div class="cart-button">
						<button class="btn-primary" type="button"  onclick="ajaxAdd($(this),47);" > <i class="fl-outicons-shopping-cart13"></i> <span>Add to Cart</span> </button>
						<div class="clear"></div>
						<button class="product-btn" type="button" onclick="wishlist.add('47');"> <i class="material-icons-favorite_border"></i> <span>Add to Wishlist</span></button>
						<button class="product-btn" type="button" onclick="compare.add('47');"> <i class="material-icons-shuffle"></i> <span>Add to Compare</span></button>
					</div>
				</div>
														<div class="product-thumb transition options">
										<!-- Product options -->
					<div class="product-option-wrap">
						<div class="product-options form-horizontal">
							<div class="options">
								<a class="ajax-overlay_close" href='#'></a>
								<h3>Available Options</h3>
								<div class="form-group hidden">
									<div class="col-sm-8">
										<input type="text" name="product_id" value="46" class="form-control"/>
									</div>
								</div>
																								<div class="form-group required">
									<label class="control-label col-sm-12" for="input-option3203005">
										Size									</label>
									<div class="col-sm-12">
										<select name="option[32]" id="input-option3203005" class="form-control">
											<option value="">--- Please Select ---</option>
																						<option value="72">
												Small												(+$20.00)&lrm;											</option>
																						<option value="73">
												Medium												(+$15.00)&lrm;											</option>
																						<option value="74">
												Large												(+$10.00)&lrm;											</option>
																					</select>
									</div>
								</div>
																																																																																																<button class="btn-primary" type="button" onclick="cart.addPopup($(this),'46');">
									<i class="fl-outicons-shopping-cart13"></i>  <span>Add to Cart</span>
								</button>
							</div>
						</div>
					</div>
										<div class="quick_info">
						<div id="quickview_latest_30050" class="quickview-style">
							<div class="left col-sm-5">
								<div class="quickview_image image">
									<a href="https://livedemo00.template-help.com/opencart_59086/index.php?route=product/product&amp;product_id=46">
										<img width="310" height="310" alt="Eye Pencil Ultra Black" title="Eye Pencil Ultra Black" class="img-responsive" src="image/cache/catalog/products/product_46-310x310.png"/>
									</a>
								</div>
							</div>
							<div class="right col-sm-7">
								<h2>Eye Pencil Ultra Black</h2>								
																<div class="rating">
																											<span class="fa-stack"> <i class="fa fa-star active fa-stack-1x"></i> </span>
																																				<span class="fa-stack"> <i class="fa fa-star active fa-stack-1x"></i> </span>
																																				<span class="fa-stack"> <i class="fa fa-star active fa-stack-1x"></i> </span>
																																				<span class="fa-stack"> <i class="fa fa-star active fa-stack-1x"></i> </span>
																																				<span class="fa-stack"> <i class="fa fa-star fa-stack-1x"></i> </span>
																										</div>
																<div class="inf">
																		<p class="quickview_manufacture manufacture manufacture">
										Brand:										<a href="https://livedemo00.template-help.com/opencart_59086/index.php?route=product/manufacturer/info&amp;manufacturer_id=30">Physicians Formula</a>
									</p>
																											<p class="product_model model">
										Model: Classic									</p>
																		
								</div>								
																	<div class="price">
																				$100.00																													</div>
																<button class="btn-primary" data-toggle="tooltip" title="Add to Cart" type="button" onclick="cart.add('46');"> <i class="fl-outicons-shopping-cart13"></i> <span>Add to Cart</span> </button>
								<ul class="product-buttons">
									<li>
										<button class="product-btn" type="button" data-toggle="tooltip" title="Add to Wishlist" onclick="wishlist.add('46');">
											<i class="material-icons-favorite_border"></i> <span>Add to Wishlist</span>
										</button>
									</li>
									<li>
										<button class="product-btn" type="button" data-toggle="tooltip" title="Add to Compare" onclick="compare.add('46');">
											<i class="material-icons-shuffle"></i> <span>Add to Compare</span>
										</button>
									</li>
								</ul>
								<div class="clear"></div>
							</div>
						</div>
					</div>
															<div class="new-pr"><span>New</span></div>
										<div class="image">
						<a class="lazy" style="padding-bottom: 100%" href="https://livedemo00.template-help.com/opencart_59086/index.php?route=product/product&amp;product_id=46">
							<img width="310" height="310" alt="Eye Pencil Ultra Black" title="Eye Pencil Ultra Black" class="img-responsive" data-src="image/cache/catalog/products/product_46-310x310.png" src="#"/>
						</a>
						<a class="quickview" data-rel="details" href="#quickview_latest_30050">
							Quick View						</a>
													<div class="rating">
																								<span class="fa-stack"> <i class="fa fa-star active fa-stack-1x"></i> </span>
																																<span class="fa-stack"> <i class="fa fa-star active fa-stack-1x"></i> </span>
																																<span class="fa-stack"> <i class="fa fa-star active fa-stack-1x"></i> </span>
																																<span class="fa-stack"> <i class="fa fa-star active fa-stack-1x"></i> </span>
																																<span class="fa-stack"> <i class="fa fa-star fa-stack-1x"></i> </span>
																							</div>
											</div>
					<div class="caption">						
						<div class="name">
							<a href="https://livedemo00.template-help.com/opencart_59086/index.php?route=product/product&amp;product_id=46">Eye Pencil Ultra Black</a>
						</div>
						<div class="category-name">
							Face 
						</div>						
												<div class="price">
														$100.00																				</div>
											</div>
					<div class="cart-button">
						<button class="btn-primary" type="button"  onclick="ajaxAdd($(this),46);" > <i class="fl-outicons-shopping-cart13"></i> <span>Add to Cart</span> </button>
						<div class="clear"></div>
						<button class="product-btn" type="button" onclick="wishlist.add('46');"> <i class="material-icons-favorite_border"></i> <span>Add to Wishlist</span></button>
						<button class="product-btn" type="button" onclick="compare.add('46');"> <i class="material-icons-shuffle"></i> <span>Add to Compare</span></button>
					</div>
				</div>
														<div class="product-thumb transition options">
										<!-- Product options -->
					<div class="product-option-wrap">
						<div class="product-options form-horizontal">
							<div class="options">
								<a class="ajax-overlay_close" href='#'></a>
								<h3>Available Options</h3>
								<div class="form-group hidden">
									<div class="col-sm-8">
										<input type="text" name="product_id" value="45" class="form-control"/>
									</div>
								</div>
																								<div class="form-group">
									<label class="control-label col-sm-12" for="input-option3003006">
										Size									</label>
									<div class="col-sm-12">
										<select name="option[30]" id="input-option3003006" class="form-control">
											<option value="">--- Please Select ---</option>
																						<option value="68">
												Small												(+$20.00)&lrm;											</option>
																						<option value="69">
												Medium												(+$15.00)&lrm;											</option>
																						<option value="70">
												Large												(+$10.00)&lrm;											</option>
																					</select>
									</div>
								</div>
																																																																																																																<div class="form-group required">
									<label class="control-label col-sm-12">
										Color									</label>
									<div class="col-sm-12">
										<div id="input-option3103006">
																						<div class="radio">
												<label for="option[317103006]">
													<input type="radio" hidden name="option[31]" id="option[317103006]" value="71"/>
													red													(+$10.00)&lrm;												</label>
											</div>
																					</div>
									</div>
								</div>
																																																																																								<button class="btn-primary" type="button" onclick="cart.addPopup($(this),'45');">
									<i class="fl-outicons-shopping-cart13"></i>  <span>Add to Cart</span>
								</button>
							</div>
						</div>
					</div>
										<div class="quick_info">
						<div id="quickview_latest_30060" class="quickview-style">
							<div class="left col-sm-5">
								<div class="quickview_image image">
									<a href="https://livedemo00.template-help.com/opencart_59086/index.php?route=product/product&amp;product_id=45">
										<img width="310" height="310" alt="Pantene Pro-V Expert Collection AgeDefy" title="Pantene Pro-V Expert Collection AgeDefy" class="img-responsive" src="image/cache/catalog/products/product_43-310x310.png"/>
									</a>
								</div>
							</div>
							<div class="right col-sm-7">
								<h2>Pantene Pro-V Expert Collection AgeDefy</h2>								
																<div class="rating">
																											<span class="fa-stack"> <i class="fa fa-star active fa-stack-1x"></i> </span>
																																				<span class="fa-stack"> <i class="fa fa-star active fa-stack-1x"></i> </span>
																																				<span class="fa-stack"> <i class="fa fa-star active fa-stack-1x"></i> </span>
																																				<span class="fa-stack"> <i class="fa fa-star active fa-stack-1x"></i> </span>
																																				<span class="fa-stack"> <i class="fa fa-star active fa-stack-1x"></i> </span>
																										</div>
																<div class="inf">
																		<p class="quickview_manufacture manufacture manufacture">
										Brand:										<a href="https://livedemo00.template-help.com/opencart_59086/index.php?route=product/manufacturer/info&amp;manufacturer_id=29">Pantene</a>
									</p>
																											<p class="product_model model">
										Model: Collectible									</p>
																		
								</div>								
																	<div class="price">
																				<span class="price-new">$88.00</span>
																													</div>
																<button class="btn-primary" data-toggle="tooltip" title="Add to Cart" type="button" onclick="cart.add('45');"> <i class="fl-outicons-shopping-cart13"></i> <span>Add to Cart</span> </button>
								<ul class="product-buttons">
									<li>
										<button class="product-btn" type="button" data-toggle="tooltip" title="Add to Wishlist" onclick="wishlist.add('45');">
											<i class="material-icons-favorite_border"></i> <span>Add to Wishlist</span>
										</button>
									</li>
									<li>
										<button class="product-btn" type="button" data-toggle="tooltip" title="Add to Compare" onclick="compare.add('45');">
											<i class="material-icons-shuffle"></i> <span>Add to Compare</span>
										</button>
									</li>
								</ul>
								<div class="clear"></div>
							</div>
						</div>
					</div>
																		<div class="sale">
								<span>Sale</span>
							</div>
																											<div class="image">
						<a class="lazy" style="padding-bottom: 100%" href="https://livedemo00.template-help.com/opencart_59086/index.php?route=product/product&amp;product_id=45">
							<img width="310" height="310" alt="Pantene Pro-V Expert Collection AgeDefy" title="Pantene Pro-V Expert Collection AgeDefy" class="img-responsive" data-src="image/cache/catalog/products/product_43-310x310.png" src="#"/>
						</a>
						<a class="quickview" data-rel="details" href="#quickview_latest_30060">
							Quick View						</a>
													<div class="rating">
																								<span class="fa-stack"> <i class="fa fa-star active fa-stack-1x"></i> </span>
																																<span class="fa-stack"> <i class="fa fa-star active fa-stack-1x"></i> </span>
																																<span class="fa-stack"> <i class="fa fa-star active fa-stack-1x"></i> </span>
																																<span class="fa-stack"> <i class="fa fa-star active fa-stack-1x"></i> </span>
																																<span class="fa-stack"> <i class="fa fa-star active fa-stack-1x"></i> </span>
																							</div>
											</div>
					<div class="caption">						
						<div class="name">
							<a href="https://livedemo00.template-help.com/opencart_59086/index.php?route=product/product&amp;product_id=45">Pantene Pro-V Expert Collection AgeDefy</a>
						</div>
						<div class="category-name">
							Perfumes 
						</div>						
												<div class="price">
														<span class="price-new">$88.00</span> 
							<span class="price-old">$110.00</span>
																				</div>
											</div>
					<div class="cart-button">
						<button class="btn-primary" type="button"  onclick="ajaxAdd($(this),45);" > <i class="fl-outicons-shopping-cart13"></i> <span>Add to Cart</span> </button>
						<div class="clear"></div>
						<button class="product-btn" type="button" onclick="wishlist.add('45');"> <i class="material-icons-favorite_border"></i> <span>Add to Wishlist</span></button>
						<button class="product-btn" type="button" onclick="compare.add('45');"> <i class="material-icons-shuffle"></i> <span>Add to Compare</span></button>
					</div>
				</div>
														<div class="product-thumb transition options">
										<!-- Product options -->
					<div class="product-option-wrap">
						<div class="product-options form-horizontal">
							<div class="options">
								<a class="ajax-overlay_close" href='#'></a>
								<h3>Available Options</h3>
								<div class="form-group hidden">
									<div class="col-sm-8">
										<input type="text" name="product_id" value="44" class="form-control"/>
									</div>
								</div>
																								<div class="form-group required">
									<label class="control-label col-sm-12" for="input-option2903007">
										Size									</label>
									<div class="col-sm-12">
										<select name="option[29]" id="input-option2903007" class="form-control">
											<option value="">--- Please Select ---</option>
																						<option value="65">
												Small												(+$20.00)&lrm;											</option>
																						<option value="66">
												Medium												(+$15.00)&lrm;											</option>
																						<option value="67">
												Large												(+$10.00)&lrm;											</option>
																					</select>
									</div>
								</div>
																																																																																																<button class="btn-primary" type="button" onclick="cart.addPopup($(this),'44');">
									<i class="fl-outicons-shopping-cart13"></i>  <span>Add to Cart</span>
								</button>
							</div>
						</div>
					</div>
										<div class="quick_info">
						<div id="quickview_latest_30070" class="quickview-style">
							<div class="left col-sm-5">
								<div class="quickview_image image">
									<a href="https://livedemo00.template-help.com/opencart_59086/index.php?route=product/product&amp;product_id=44">
										<img width="310" height="310" alt="2-In-1 Shampoo &amp; Conditioner" title="2-In-1 Shampoo &amp; Conditioner" class="img-responsive" src="image/cache/catalog/products/product_40-310x310.png"/>
									</a>
								</div>
							</div>
							<div class="right col-sm-7">
								<h2>2-In-1 Shampoo &amp; Conditioner</h2>								
																<div class="inf">
																		<p class="quickview_manufacture manufacture manufacture">
										Brand:										<a href="https://livedemo00.template-help.com/opencart_59086/index.php?route=product/manufacturer/info&amp;manufacturer_id=28">Olay</a>
									</p>
																											<p class="product_model model">
										Model: Deluxe									</p>
																		
								</div>								
																	<div class="price">
																				<span class="price-new">$64.00</span>
																													</div>
																<button class="btn-primary" data-toggle="tooltip" title="Add to Cart" type="button" onclick="cart.add('44');"> <i class="fl-outicons-shopping-cart13"></i> <span>Add to Cart</span> </button>
								<ul class="product-buttons">
									<li>
										<button class="product-btn" type="button" data-toggle="tooltip" title="Add to Wishlist" onclick="wishlist.add('44');">
											<i class="material-icons-favorite_border"></i> <span>Add to Wishlist</span>
										</button>
									</li>
									<li>
										<button class="product-btn" type="button" data-toggle="tooltip" title="Add to Compare" onclick="compare.add('44');">
											<i class="material-icons-shuffle"></i> <span>Add to Compare</span>
										</button>
									</li>
								</ul>
								<div class="clear"></div>
							</div>
						</div>
					</div>
																		<div class="sale">
								<span>Sale</span>
							</div>
																											<div class="image">
						<a class="lazy" style="padding-bottom: 100%" href="https://livedemo00.template-help.com/opencart_59086/index.php?route=product/product&amp;product_id=44">
							<img width="310" height="310" alt="2-In-1 Shampoo &amp; Conditioner" title="2-In-1 Shampoo &amp; Conditioner" class="img-responsive" data-src="image/cache/catalog/products/product_40-310x310.png" src="#"/>
						</a>
						<a class="quickview" data-rel="details" href="#quickview_latest_30070">
							Quick View						</a>
											</div>
					<div class="caption">						
						<div class="name">
							<a href="https://livedemo00.template-help.com/opencart_59086/index.php?route=product/product&amp;product_id=44">2-In-1 Shampoo &amp; Conditioner</a>
						</div>
						<div class="category-name">
							Hair 
						</div>						
												<div class="price">
														<span class="price-new">$64.00</span> 
							<span class="price-old">$80.00</span>
																				</div>
											</div>
					<div class="cart-button">
						<button class="btn-primary" type="button"  onclick="ajaxAdd($(this),44);" > <i class="fl-outicons-shopping-cart13"></i> <span>Add to Cart</span> </button>
						<div class="clear"></div>
						<button class="product-btn" type="button" onclick="wishlist.add('44');"> <i class="material-icons-favorite_border"></i> <span>Add to Wishlist</span></button>
						<button class="product-btn" type="button" onclick="compare.add('44');"> <i class="material-icons-shuffle"></i> <span>Add to Compare</span></button>
					</div>
				</div>
														<div class="product-thumb transition options">
										<!-- Product options -->
					<div class="product-option-wrap">
						<div class="product-options form-horizontal">
							<div class="options">
								<a class="ajax-overlay_close" href='#'></a>
								<h3>Available Options</h3>
								<div class="form-group hidden">
									<div class="col-sm-8">
										<input type="text" name="product_id" value="43" class="form-control"/>
									</div>
								</div>
																								<div class="form-group required">
									<label class="control-label col-sm-12" for="input-option2703008">
										Size									</label>
									<div class="col-sm-12">
										<select name="option[27]" id="input-option2703008" class="form-control">
											<option value="">--- Please Select ---</option>
																						<option value="61">
												Small												(+$20.00)&lrm;											</option>
																						<option value="62">
												Medium												(+$15.00)&lrm;											</option>
																						<option value="63">
												Large												(+$10.00)&lrm;											</option>
																					</select>
									</div>
								</div>
																																																																																																																<div class="form-group required">
									<label class="control-label col-sm-12">
										Color									</label>
									<div class="col-sm-12">
										<div id="input-option2803008">
																						<div class="radio">
												<label for="option[286403008]">
													<input type="radio" hidden name="option[28]" id="option[286403008]" value="64"/>
													red													(+$10.00)&lrm;												</label>
											</div>
																					</div>
									</div>
								</div>
																																																																																								<button class="btn-primary" type="button" onclick="cart.addPopup($(this),'43');">
									<i class="fl-outicons-shopping-cart13"></i>  <span>Add to Cart</span>
								</button>
							</div>
						</div>
					</div>
										<div class="quick_info">
						<div id="quickview_latest_30080" class="quickview-style">
							<div class="left col-sm-5">
								<div class="quickview_image image">
									<a href="https://livedemo00.template-help.com/opencart_59086/index.php?route=product/product&amp;product_id=43">
										<img width="310" height="310" alt="Anti-Aging Daily Moisturizer" title="Anti-Aging Daily Moisturizer" class="img-responsive" src="image/cache/catalog/products/product_37-310x310.png"/>
									</a>
								</div>
							</div>
							<div class="right col-sm-7">
								<h2>Anti-Aging Daily Moisturizer</h2>								
																<div class="rating">
																											<span class="fa-stack"> <i class="fa fa-star active fa-stack-1x"></i> </span>
																																				<span class="fa-stack"> <i class="fa fa-star active fa-stack-1x"></i> </span>
																																				<span class="fa-stack"> <i class="fa fa-star active fa-stack-1x"></i> </span>
																																				<span class="fa-stack"> <i class="fa fa-star active fa-stack-1x"></i> </span>
																																				<span class="fa-stack"> <i class="fa fa-star fa-stack-1x"></i> </span>
																										</div>
																<div class="inf">
																		<p class="quickview_manufacture manufacture manufacture">
										Brand:										<a href="https://livedemo00.template-help.com/opencart_59086/index.php?route=product/manufacturer/info&amp;manufacturer_id=27">Nivea</a>
									</p>
																											<p class="product_model model">
										Model: Premium									</p>
																		
								</div>								
																	<div class="price">
																				<span class="price-new">$64.00</span>
																													</div>
																<button class="btn-primary" data-toggle="tooltip" title="Add to Cart" type="button" onclick="cart.add('43');"> <i class="fl-outicons-shopping-cart13"></i> <span>Add to Cart</span> </button>
								<ul class="product-buttons">
									<li>
										<button class="product-btn" type="button" data-toggle="tooltip" title="Add to Wishlist" onclick="wishlist.add('43');">
											<i class="material-icons-favorite_border"></i> <span>Add to Wishlist</span>
										</button>
									</li>
									<li>
										<button class="product-btn" type="button" data-toggle="tooltip" title="Add to Compare" onclick="compare.add('43');">
											<i class="material-icons-shuffle"></i> <span>Add to Compare</span>
										</button>
									</li>
								</ul>
								<div class="clear"></div>
							</div>
						</div>
					</div>
																		<div class="sale">
								<span>Sale</span>
							</div>
																											<div class="image">
						<a class="lazy" style="padding-bottom: 100%" href="https://livedemo00.template-help.com/opencart_59086/index.php?route=product/product&amp;product_id=43">
							<img width="310" height="310" alt="Anti-Aging Daily Moisturizer" title="Anti-Aging Daily Moisturizer" class="img-responsive" data-src="image/cache/catalog/products/product_37-310x310.png" src="#"/>
						</a>
						<a class="quickview" data-rel="details" href="#quickview_latest_30080">
							Quick View						</a>
													<div class="rating">
																								<span class="fa-stack"> <i class="fa fa-star active fa-stack-1x"></i> </span>
																																<span class="fa-stack"> <i class="fa fa-star active fa-stack-1x"></i> </span>
																																<span class="fa-stack"> <i class="fa fa-star active fa-stack-1x"></i> </span>
																																<span class="fa-stack"> <i class="fa fa-star active fa-stack-1x"></i> </span>
																																<span class="fa-stack"> <i class="fa fa-star fa-stack-1x"></i> </span>
																							</div>
											</div>
					<div class="caption">						
						<div class="name">
							<a href="https://livedemo00.template-help.com/opencart_59086/index.php?route=product/product&amp;product_id=43">Anti-Aging Daily Moisturizer</a>
						</div>
						<div class="category-name">
							Face 
						</div>						
												<div class="price">
														<span class="price-new">$64.00</span> 
							<span class="price-old">$80.00</span>
																				</div>
											</div>
					<div class="cart-button">
						<button class="btn-primary" type="button"  onclick="ajaxAdd($(this),43);" > <i class="fl-outicons-shopping-cart13"></i> <span>Add to Cart</span> </button>
						<div class="clear"></div>
						<button class="product-btn" type="button" onclick="wishlist.add('43');"> <i class="material-icons-favorite_border"></i> <span>Add to Wishlist</span></button>
						<button class="product-btn" type="button" onclick="compare.add('43');"> <i class="material-icons-shuffle"></i> <span>Add to Compare</span></button>
					</div>
				</div>
									</div>
	</div>
</div>
<div class="box featured">
<div class="clear"></div>
	<div class="box-heading">
		<h3>Featured Products</h3>
	</div>
	<div class="box-content">
		<div class="box-carousel">
										<div class="product-thumb transition options">
										<!-- Product options -->
					<div class="product-option-wrap">
						<div class="product-options form-horizontal">
							<div class="options">
								<a class="ajax-overlay_close" href='#'></a>
								<h3>Available Options</h3>
								<div class="form-group hidden">
									<div class="col-sm-8">
										<input type="text" name="product_id" value="34" class="form-control"/>
									</div>
								</div>
																								<div class="form-group required">
									<label class="control-label col-sm-12" for="input-option1202001">
										Size									</label>
									<div class="col-sm-12">
										<select name="option[12]" id="input-option1202001" class="form-control">
											<option value="">--- Please Select ---</option>
																						<option value="27">
												Small												(+$20.00)&lrm;											</option>
																						<option value="28">
												Medium												(+$15.00)&lrm;											</option>
																						<option value="29">
												Large												(+$10.00)&lrm;											</option>
																					</select>
									</div>
								</div>
																																																																																																																<div class="form-group required">
									<label class="control-label col-sm-12">
										Color									</label>
									<div class="col-sm-12">
										<div id="input-option1302001">
																						<div class="radio">
												<label for="option[133002001]">
													<input type="radio" hidden name="option[13]" id="option[133002001]" value="30"/>
													red													(+$10.00)&lrm;												</label>
											</div>
																					</div>
									</div>
								</div>
																																																																																								<button class="btn-primary" type="button" onclick="cart.addPopup($(this),'34');">
									<i class="fl-outicons-shopping-cart13"></i>  <span>Add to Cart</span>
								</button>
							</div>
						</div>
					</div>
										<div class="quick_info">
						<div id="quickview_20010" class="quickview-style">
							<div class="left col-sm-5">
								<div class="quickview_image image">
									<a href="https://livedemo00.template-help.com/opencart_59086/index.php?route=product/product&amp;product_id=34">
										<img width="310" height="310" alt="Julep Nail Polish  Color" title="Julep Nail Polish  Color" class="img-responsive" src="image/cache/catalog/products/product_19-310x310.png"/>
									</a>
								</div>
							</div>
							<div class="right col-sm-7">
								<h2>Julep Nail Polish  Color</h2>								
																<div class="rating">
																											<span class="fa-stack"> <i class="fa fa-star active fa-stack-1x"></i> </span>
																																				<span class="fa-stack"> <i class="fa fa-star active fa-stack-1x"></i> </span>
																																				<span class="fa-stack"> <i class="fa fa-star active fa-stack-1x"></i> </span>
																																				<span class="fa-stack"> <i class="fa fa-star active fa-stack-1x"></i> </span>
																																				<span class="fa-stack"> <i class="fa fa-star active fa-stack-1x"></i> </span>
																										</div>
																<div class="inf">
																		<p class="quickview_manufacture manufacture manufacture">
										Brand:										<a href="https://livedemo00.template-help.com/opencart_59086/index.php?route=product/manufacturer/info&amp;manufacturer_id=31">Shany</a>
									</p>
																											<p class="product_model model">
										Model: Classic									</p>
																		
								</div>								
																	<div class="price">
																				$90.00																													</div>
																<button class="btn-primary" data-toggle="tooltip" title="Add to Cart" type="button" onclick="cart.add('34');"> <i class="fl-outicons-shopping-cart13"></i> <span>Add to Cart</span> </button>
								<ul class="product-buttons">
									<li>
										<button class="product-btn" type="button" data-toggle="tooltip" title="Add to Wishlist" onclick="wishlist.add('34');">
											<i class="material-icons-favorite_border"></i> <span>Add to Wishlist</span>
										</button>
									</li>
									<li>
										<button class="product-btn" type="button" data-toggle="tooltip" title="Add to Compare" onclick="compare.add('34');">
											<i class="material-icons-shuffle"></i> <span>Add to Compare</span>
										</button>
									</li>
								</ul>
								<div class="clear"></div>
							</div>
						</div>
					</div>
															<div class="image">
						<a class="lazy" style="padding-bottom: 100%" href="https://livedemo00.template-help.com/opencart_59086/index.php?route=product/product&amp;product_id=34">
							<img width="310" height="310" alt="Julep Nail Polish  Color" title="Julep Nail Polish  Color" class="img-responsive" data-src="image/cache/catalog/products/product_19-310x310.png" src="#"/>
						</a>
						<a class="quickview" data-rel="details" href="#quickview_20010">
							Quick View						</a>
													<div class="rating">
																								<span class="fa-stack"> <i class="fa fa-star active fa-stack-1x"></i> </span>
																																<span class="fa-stack"> <i class="fa fa-star active fa-stack-1x"></i> </span>
																																<span class="fa-stack"> <i class="fa fa-star active fa-stack-1x"></i> </span>
																																<span class="fa-stack"> <i class="fa fa-star active fa-stack-1x"></i> </span>
																																<span class="fa-stack"> <i class="fa fa-star active fa-stack-1x"></i> </span>
																							</div>
											</div>
					<div class="caption">						
						<div class="name">
							<a href="https://livedemo00.template-help.com/opencart_59086/index.php?route=product/product&amp;product_id=34">Julep Nail Polish  Color</a>
						</div>
						<div class="category-name">
							Face 
						</div>						
												<div class="price">
														$90.00																				</div>
											</div>
					<div class="cart-button">
						<button class="btn-primary" type="button"  onclick="ajaxAdd($(this),34);" > <i class="fl-outicons-shopping-cart13"></i> <span>Add to Cart</span> </button>
						<div class="clear"></div>
						<button class="product-btn" type="button" onclick="wishlist.add('34');"> <i class="material-icons-favorite_border"></i> <span>Add to Wishlist</span></button>
						<button class="product-btn" type="button" onclick="compare.add('34');"> <i class="material-icons-shuffle"></i> <span>Add to Compare</span></button>
					</div>
				</div>
														<div class="product-thumb transition options">
										<!-- Product options -->
					<div class="product-option-wrap">
						<div class="product-options form-horizontal">
							<div class="options">
								<a class="ajax-overlay_close" href='#'></a>
								<h3>Available Options</h3>
								<div class="form-group hidden">
									<div class="col-sm-8">
										<input type="text" name="product_id" value="29" class="form-control"/>
									</div>
								</div>
																								<div class="form-group required">
									<label class="control-label col-sm-12" for="input-option302002">
										Size									</label>
									<div class="col-sm-12">
										<select name="option[3]" id="input-option302002" class="form-control">
											<option value="">--- Please Select ---</option>
																						<option value="7">
												Small												(+$15.00)&lrm;											</option>
																						<option value="8">
												Medium												(+$10.00)&lrm;											</option>
																						<option value="9">
												Large												(+$20.00)&lrm;											</option>
																					</select>
									</div>
								</div>
																																																																																																																<div class="form-group required">
									<label class="control-label col-sm-12">
										Color									</label>
									<div class="col-sm-12">
										<div id="input-option402002">
																						<div class="radio">
												<label for="option[41002002]">
													<input type="radio" hidden name="option[4]" id="option[41002002]" value="10"/>
													red																									</label>
											</div>
																					</div>
									</div>
								</div>
																																																																																								<button class="btn-primary" type="button" onclick="cart.addPopup($(this),'29');">
									<i class="fl-outicons-shopping-cart13"></i>  <span>Add to Cart</span>
								</button>
							</div>
						</div>
					</div>
										<div class="quick_info">
						<div id="quickview_20020" class="quickview-style">
							<div class="left col-sm-5">
								<div class="quickview_image image">
									<a href="https://livedemo00.template-help.com/opencart_59086/index.php?route=product/product&amp;product_id=29">
										<img width="310" height="310" alt="Borghese B Moisture  Advanced Care" title="Borghese B Moisture  Advanced Care" class="img-responsive" src="image/cache/catalog/products/product_4-310x310.png"/>
									</a>
								</div>
							</div>
							<div class="right col-sm-7">
								<h2>Borghese B Moisture  Advanced Care</h2>								
																<div class="rating">
																											<span class="fa-stack"> <i class="fa fa-star active fa-stack-1x"></i> </span>
																																				<span class="fa-stack"> <i class="fa fa-star active fa-stack-1x"></i> </span>
																																				<span class="fa-stack"> <i class="fa fa-star active fa-stack-1x"></i> </span>
																																				<span class="fa-stack"> <i class="fa fa-star active fa-stack-1x"></i> </span>
																																				<span class="fa-stack"> <i class="fa fa-star active fa-stack-1x"></i> </span>
																										</div>
																<div class="inf">
																		<p class="quickview_manufacture manufacture manufacture">
										Brand:										<a href="https://livedemo00.template-help.com/opencart_59086/index.php?route=product/manufacturer/info&amp;manufacturer_id=26">Loreal</a>
									</p>
																											<p class="product_model model">
										Model: Deluxe									</p>
																		
								</div>								
																	<div class="price">
																				<span class="price-new">$56.00</span>
																													</div>
																<button class="btn-primary" data-toggle="tooltip" title="Add to Cart" type="button" onclick="cart.add('29');"> <i class="fl-outicons-shopping-cart13"></i> <span>Add to Cart</span> </button>
								<ul class="product-buttons">
									<li>
										<button class="product-btn" type="button" data-toggle="tooltip" title="Add to Wishlist" onclick="wishlist.add('29');">
											<i class="material-icons-favorite_border"></i> <span>Add to Wishlist</span>
										</button>
									</li>
									<li>
										<button class="product-btn" type="button" data-toggle="tooltip" title="Add to Compare" onclick="compare.add('29');">
											<i class="material-icons-shuffle"></i> <span>Add to Compare</span>
										</button>
									</li>
								</ul>
								<div class="clear"></div>
							</div>
						</div>
					</div>
															<div class="sale">
						<span>Sale</span>
					</div>
																									<div class="image">
						<a class="lazy" style="padding-bottom: 100%" href="https://livedemo00.template-help.com/opencart_59086/index.php?route=product/product&amp;product_id=29">
							<img width="310" height="310" alt="Borghese B Moisture  Advanced Care" title="Borghese B Moisture  Advanced Care" class="img-responsive" data-src="image/cache/catalog/products/product_4-310x310.png" src="#"/>
						</a>
						<a class="quickview" data-rel="details" href="#quickview_20020">
							Quick View						</a>
													<div class="rating">
																								<span class="fa-stack"> <i class="fa fa-star active fa-stack-1x"></i> </span>
																																<span class="fa-stack"> <i class="fa fa-star active fa-stack-1x"></i> </span>
																																<span class="fa-stack"> <i class="fa fa-star active fa-stack-1x"></i> </span>
																																<span class="fa-stack"> <i class="fa fa-star active fa-stack-1x"></i> </span>
																																<span class="fa-stack"> <i class="fa fa-star active fa-stack-1x"></i> </span>
																							</div>
											</div>
					<div class="caption">						
						<div class="name">
							<a href="https://livedemo00.template-help.com/opencart_59086/index.php?route=product/product&amp;product_id=29">Borghese B Moisture  Advanced Care</a>
						</div>
						<div class="category-name">
							Face 
						</div>						
												<div class="price">
														<span class="price-new">$56.00</span> 
							<span class="price-old">$70.00</span>
																				</div>
											</div>
					<div class="cart-button">
						<button class="btn-primary" type="button"  onclick="ajaxAdd($(this),29);" > <i class="fl-outicons-shopping-cart13"></i> <span>Add to Cart</span> </button>
						<div class="clear"></div>
						<button class="product-btn" type="button" onclick="wishlist.add('29');"> <i class="material-icons-favorite_border"></i> <span>Add to Wishlist</span></button>
						<button class="product-btn" type="button" onclick="compare.add('29');"> <i class="material-icons-shuffle"></i> <span>Add to Compare</span></button>
					</div>
				</div>
														<div class="product-thumb transition options">
										<!-- Product options -->
					<div class="product-option-wrap">
						<div class="product-options form-horizontal">
							<div class="options">
								<a class="ajax-overlay_close" href='#'></a>
								<h3>Available Options</h3>
								<div class="form-group hidden">
									<div class="col-sm-8">
										<input type="text" name="product_id" value="31" class="form-control"/>
									</div>
								</div>
																								<div class="form-group required">
									<label class="control-label col-sm-12" for="input-option702003">
										Size									</label>
									<div class="col-sm-12">
										<select name="option[7]" id="input-option702003" class="form-control">
											<option value="">--- Please Select ---</option>
																						<option value="17">
												Small												(+$20.00)&lrm;											</option>
																						<option value="18">
												Medium												(+$15.00)&lrm;											</option>
																						<option value="19">
												Large												(+$10.00)&lrm;											</option>
																					</select>
									</div>
								</div>
																																																																																																																<div class="form-group required">
									<label class="control-label col-sm-12">
										Color									</label>
									<div class="col-sm-12">
										<div id="input-option802003">
																						<div class="radio">
												<label for="option[82002003]">
													<input type="radio" hidden name="option[8]" id="option[82002003]" value="20"/>
													red													(+$10.00)&lrm;												</label>
											</div>
																					</div>
									</div>
								</div>
																																																																																								<button class="btn-primary" type="button" onclick="cart.addPopup($(this),'31');">
									<i class="fl-outicons-shopping-cart13"></i>  <span>Add to Cart</span>
								</button>
							</div>
						</div>
					</div>
										<div class="quick_info">
						<div id="quickview_20030" class="quickview-style">
							<div class="left col-sm-5">
								<div class="quickview_image image">
									<a href="https://livedemo00.template-help.com/opencart_59086/index.php?route=product/product&amp;product_id=31">
										<img width="310" height="310" alt="butter LONDON LIPPY Moisture Matte Lipstick" title="butter LONDON LIPPY Moisture Matte Lipstick" class="img-responsive" src="image/cache/catalog/products/product_10-310x310.png"/>
									</a>
								</div>
							</div>
							<div class="right col-sm-7">
								<h2>butter LONDON LIPPY Moisture Matte Lipstick</h2>								
																<div class="rating">
																											<span class="fa-stack"> <i class="fa fa-star active fa-stack-1x"></i> </span>
																																				<span class="fa-stack"> <i class="fa fa-star active fa-stack-1x"></i> </span>
																																				<span class="fa-stack"> <i class="fa fa-star active fa-stack-1x"></i> </span>
																																				<span class="fa-stack"> <i class="fa fa-star active fa-stack-1x"></i> </span>
																																				<span class="fa-stack"> <i class="fa fa-star fa-stack-1x"></i> </span>
																										</div>
																<div class="inf">
																		<p class="quickview_manufacture manufacture manufacture">
										Brand:										<a href="https://livedemo00.template-help.com/opencart_59086/index.php?route=product/manufacturer/info&amp;manufacturer_id=28">Olay</a>
									</p>
																											<p class="product_model model">
										Model: Collectible									</p>
																		
								</div>								
																	<div class="price">
																				$70.00																													</div>
																<button class="btn-primary" data-toggle="tooltip" title="Add to Cart" type="button" onclick="cart.add('31');"> <i class="fl-outicons-shopping-cart13"></i> <span>Add to Cart</span> </button>
								<ul class="product-buttons">
									<li>
										<button class="product-btn" type="button" data-toggle="tooltip" title="Add to Wishlist" onclick="wishlist.add('31');">
											<i class="material-icons-favorite_border"></i> <span>Add to Wishlist</span>
										</button>
									</li>
									<li>
										<button class="product-btn" type="button" data-toggle="tooltip" title="Add to Compare" onclick="compare.add('31');">
											<i class="material-icons-shuffle"></i> <span>Add to Compare</span>
										</button>
									</li>
								</ul>
								<div class="clear"></div>
							</div>
						</div>
					</div>
															<div class="image">
						<a class="lazy" style="padding-bottom: 100%" href="https://livedemo00.template-help.com/opencart_59086/index.php?route=product/product&amp;product_id=31">
							<img width="310" height="310" alt="butter LONDON LIPPY Moisture Matte Lipstick" title="butter LONDON LIPPY Moisture Matte Lipstick" class="img-responsive" data-src="image/cache/catalog/products/product_10-310x310.png" src="#"/>
						</a>
						<a class="quickview" data-rel="details" href="#quickview_20030">
							Quick View						</a>
													<div class="rating">
																								<span class="fa-stack"> <i class="fa fa-star active fa-stack-1x"></i> </span>
																																<span class="fa-stack"> <i class="fa fa-star active fa-stack-1x"></i> </span>
																																<span class="fa-stack"> <i class="fa fa-star active fa-stack-1x"></i> </span>
																																<span class="fa-stack"> <i class="fa fa-star active fa-stack-1x"></i> </span>
																																<span class="fa-stack"> <i class="fa fa-star fa-stack-1x"></i> </span>
																							</div>
											</div>
					<div class="caption">						
						<div class="name">
							<a href="https://livedemo00.template-help.com/opencart_59086/index.php?route=product/product&amp;product_id=31">butter LONDON LIPPY Moisture Matte Lipstick</a>
						</div>
						<div class="category-name">
							Face 
						</div>						
												<div class="price">
														$70.00																				</div>
											</div>
					<div class="cart-button">
						<button class="btn-primary" type="button"  onclick="ajaxAdd($(this),31);" > <i class="fl-outicons-shopping-cart13"></i> <span>Add to Cart</span> </button>
						<div class="clear"></div>
						<button class="product-btn" type="button" onclick="wishlist.add('31');"> <i class="material-icons-favorite_border"></i> <span>Add to Wishlist</span></button>
						<button class="product-btn" type="button" onclick="compare.add('31');"> <i class="material-icons-shuffle"></i> <span>Add to Compare</span></button>
					</div>
				</div>
														<div class="product-thumb transition options">
										<!-- Product options -->
					<div class="product-option-wrap">
						<div class="product-options form-horizontal">
							<div class="options">
								<a class="ajax-overlay_close" href='#'></a>
								<h3>Available Options</h3>
								<div class="form-group hidden">
									<div class="col-sm-8">
										<input type="text" name="product_id" value="45" class="form-control"/>
									</div>
								</div>
																								<div class="form-group">
									<label class="control-label col-sm-12" for="input-option3002004">
										Size									</label>
									<div class="col-sm-12">
										<select name="option[30]" id="input-option3002004" class="form-control">
											<option value="">--- Please Select ---</option>
																						<option value="68">
												Small												(+$20.00)&lrm;											</option>
																						<option value="69">
												Medium												(+$15.00)&lrm;											</option>
																						<option value="70">
												Large												(+$10.00)&lrm;											</option>
																					</select>
									</div>
								</div>
																																																																																																																<div class="form-group required">
									<label class="control-label col-sm-12">
										Color									</label>
									<div class="col-sm-12">
										<div id="input-option3102004">
																						<div class="radio">
												<label for="option[317102004]">
													<input type="radio" hidden name="option[31]" id="option[317102004]" value="71"/>
													red													(+$10.00)&lrm;												</label>
											</div>
																					</div>
									</div>
								</div>
																																																																																								<button class="btn-primary" type="button" onclick="cart.addPopup($(this),'45');">
									<i class="fl-outicons-shopping-cart13"></i>  <span>Add to Cart</span>
								</button>
							</div>
						</div>
					</div>
										<div class="quick_info">
						<div id="quickview_20040" class="quickview-style">
							<div class="left col-sm-5">
								<div class="quickview_image image">
									<a href="https://livedemo00.template-help.com/opencart_59086/index.php?route=product/product&amp;product_id=45">
										<img width="310" height="310" alt="Pantene Pro-V Expert Collection AgeDefy" title="Pantene Pro-V Expert Collection AgeDefy" class="img-responsive" src="image/cache/catalog/products/product_43-310x310.png"/>
									</a>
								</div>
							</div>
							<div class="right col-sm-7">
								<h2>Pantene Pro-V Expert Collection AgeDefy</h2>								
																<div class="rating">
																											<span class="fa-stack"> <i class="fa fa-star active fa-stack-1x"></i> </span>
																																				<span class="fa-stack"> <i class="fa fa-star active fa-stack-1x"></i> </span>
																																				<span class="fa-stack"> <i class="fa fa-star active fa-stack-1x"></i> </span>
																																				<span class="fa-stack"> <i class="fa fa-star active fa-stack-1x"></i> </span>
																																				<span class="fa-stack"> <i class="fa fa-star active fa-stack-1x"></i> </span>
																										</div>
																<div class="inf">
																		<p class="quickview_manufacture manufacture manufacture">
										Brand:										<a href="https://livedemo00.template-help.com/opencart_59086/index.php?route=product/manufacturer/info&amp;manufacturer_id=29">Pantene</a>
									</p>
																											<p class="product_model model">
										Model: Collectible									</p>
																		
								</div>								
																	<div class="price">
																				<span class="price-new">$88.00</span>
																													</div>
																<button class="btn-primary" data-toggle="tooltip" title="Add to Cart" type="button" onclick="cart.add('45');"> <i class="fl-outicons-shopping-cart13"></i> <span>Add to Cart</span> </button>
								<ul class="product-buttons">
									<li>
										<button class="product-btn" type="button" data-toggle="tooltip" title="Add to Wishlist" onclick="wishlist.add('45');">
											<i class="material-icons-favorite_border"></i> <span>Add to Wishlist</span>
										</button>
									</li>
									<li>
										<button class="product-btn" type="button" data-toggle="tooltip" title="Add to Compare" onclick="compare.add('45');">
											<i class="material-icons-shuffle"></i> <span>Add to Compare</span>
										</button>
									</li>
								</ul>
								<div class="clear"></div>
							</div>
						</div>
					</div>
															<div class="sale">
						<span>Sale</span>
					</div>
																									<div class="image">
						<a class="lazy" style="padding-bottom: 100%" href="https://livedemo00.template-help.com/opencart_59086/index.php?route=product/product&amp;product_id=45">
							<img width="310" height="310" alt="Pantene Pro-V Expert Collection AgeDefy" title="Pantene Pro-V Expert Collection AgeDefy" class="img-responsive" data-src="image/cache/catalog/products/product_43-310x310.png" src="#"/>
						</a>
						<a class="quickview" data-rel="details" href="#quickview_20040">
							Quick View						</a>
													<div class="rating">
																								<span class="fa-stack"> <i class="fa fa-star active fa-stack-1x"></i> </span>
																																<span class="fa-stack"> <i class="fa fa-star active fa-stack-1x"></i> </span>
																																<span class="fa-stack"> <i class="fa fa-star active fa-stack-1x"></i> </span>
																																<span class="fa-stack"> <i class="fa fa-star active fa-stack-1x"></i> </span>
																																<span class="fa-stack"> <i class="fa fa-star active fa-stack-1x"></i> </span>
																							</div>
											</div>
					<div class="caption">						
						<div class="name">
							<a href="https://livedemo00.template-help.com/opencart_59086/index.php?route=product/product&amp;product_id=45">Pantene Pro-V Expert Collection AgeDefy</a>
						</div>
						<div class="category-name">
							Perfumes 
						</div>						
												<div class="price">
														<span class="price-new">$88.00</span> 
							<span class="price-old">$110.00</span>
																				</div>
											</div>
					<div class="cart-button">
						<button class="btn-primary" type="button"  onclick="ajaxAdd($(this),45);" > <i class="fl-outicons-shopping-cart13"></i> <span>Add to Cart</span> </button>
						<div class="clear"></div>
						<button class="product-btn" type="button" onclick="wishlist.add('45');"> <i class="material-icons-favorite_border"></i> <span>Add to Wishlist</span></button>
						<button class="product-btn" type="button" onclick="compare.add('45');"> <i class="material-icons-shuffle"></i> <span>Add to Compare</span></button>
					</div>
				</div>
														<div class="product-thumb transition options">
										<!-- Product options -->
					<div class="product-option-wrap">
						<div class="product-options form-horizontal">
							<div class="options">
								<a class="ajax-overlay_close" href='#'></a>
								<h3>Available Options</h3>
								<div class="form-group hidden">
									<div class="col-sm-8">
										<input type="text" name="product_id" value="49" class="form-control"/>
									</div>
								</div>
																								<div class="form-group required">
									<label class="control-label col-sm-12" for="input-option3702005">
										Size									</label>
									<div class="col-sm-12">
										<select name="option[37]" id="input-option3702005" class="form-control">
											<option value="">--- Please Select ---</option>
																						<option value="83">
												Small												(+$20.00)&lrm;											</option>
																						<option value="84">
												Medium												(+$15.00)&lrm;											</option>
																						<option value="85">
												Large												(+$10.00)&lrm;											</option>
																					</select>
									</div>
								</div>
																																																																																																																<div class="form-group required">
									<label class="control-label col-sm-12">
										Color									</label>
									<div class="col-sm-12">
										<div id="input-option3802005">
																						<div class="radio">
												<label for="option[388602005]">
													<input type="radio" hidden name="option[38]" id="option[388602005]" value="86"/>
													red													(+$10.00)&lrm;												</label>
											</div>
																					</div>
									</div>
								</div>
																																																																																								<button class="btn-primary" type="button" onclick="cart.addPopup($(this),'49');">
									<i class="fl-outicons-shopping-cart13"></i>  <span>Add to Cart</span>
								</button>
							</div>
						</div>
					</div>
										<div class="quick_info">
						<div id="quickview_20050" class="quickview-style">
							<div class="left col-sm-5">
								<div class="quickview_image image">
									<a href="https://livedemo00.template-help.com/opencart_59086/index.php?route=product/product&amp;product_id=49">
										<img width="310" height="310" alt="VERAttiva Face Tonic Rinse and Refresher 7-Ounce" title="VERAttiva Face Tonic Rinse and Refresher 7-Ounce" class="img-responsive" src="image/cache/catalog/products/product_55-310x310.png"/>
									</a>
								</div>
							</div>
							<div class="right col-sm-7">
								<h2>VERAttiva Face Tonic Rinse and Refresher 7-Ounce</h2>								
																<div class="rating">
																											<span class="fa-stack"> <i class="fa fa-star active fa-stack-1x"></i> </span>
																																				<span class="fa-stack"> <i class="fa fa-star active fa-stack-1x"></i> </span>
																																				<span class="fa-stack"> <i class="fa fa-star active fa-stack-1x"></i> </span>
																																				<span class="fa-stack"> <i class="fa fa-star active fa-stack-1x"></i> </span>
																																				<span class="fa-stack"> <i class="fa fa-star active fa-stack-1x"></i> </span>
																										</div>
																<div class="inf">
																		<p class="quickview_manufacture manufacture manufacture">
										Brand:										<a href="https://livedemo00.template-help.com/opencart_59086/index.php?route=product/manufacturer/info&amp;manufacturer_id=33">Clarins</a>
									</p>
																											<p class="product_model model">
										Model: Deluxe									</p>
																		
								</div>								
																	<div class="price">
																				<span class="price-new">$56.00</span>
																													</div>
																<button class="btn-primary" data-toggle="tooltip" title="Add to Cart" type="button" onclick="cart.add('49');"> <i class="fl-outicons-shopping-cart13"></i> <span>Add to Cart</span> </button>
								<ul class="product-buttons">
									<li>
										<button class="product-btn" type="button" data-toggle="tooltip" title="Add to Wishlist" onclick="wishlist.add('49');">
											<i class="material-icons-favorite_border"></i> <span>Add to Wishlist</span>
										</button>
									</li>
									<li>
										<button class="product-btn" type="button" data-toggle="tooltip" title="Add to Compare" onclick="compare.add('49');">
											<i class="material-icons-shuffle"></i> <span>Add to Compare</span>
										</button>
									</li>
								</ul>
								<div class="clear"></div>
							</div>
						</div>
					</div>
															<div class="sale">
						<span>Sale</span>
					</div>
																									<div class="image">
						<a class="lazy" style="padding-bottom: 100%" href="https://livedemo00.template-help.com/opencart_59086/index.php?route=product/product&amp;product_id=49">
							<img width="310" height="310" alt="VERAttiva Face Tonic Rinse and Refresher 7-Ounce" title="VERAttiva Face Tonic Rinse and Refresher 7-Ounce" class="img-responsive" data-src="image/cache/catalog/products/product_55-310x310.png" src="#"/>
						</a>
						<a class="quickview" data-rel="details" href="#quickview_20050">
							Quick View						</a>
													<div class="rating">
																								<span class="fa-stack"> <i class="fa fa-star active fa-stack-1x"></i> </span>
																																<span class="fa-stack"> <i class="fa fa-star active fa-stack-1x"></i> </span>
																																<span class="fa-stack"> <i class="fa fa-star active fa-stack-1x"></i> </span>
																																<span class="fa-stack"> <i class="fa fa-star active fa-stack-1x"></i> </span>
																																<span class="fa-stack"> <i class="fa fa-star active fa-stack-1x"></i> </span>
																							</div>
											</div>
					<div class="caption">						
						<div class="name">
							<a href="https://livedemo00.template-help.com/opencart_59086/index.php?route=product/product&amp;product_id=49">VERAttiva Face Tonic Rinse and Refresher 7-Ounce</a>
						</div>
						<div class="category-name">
							Face 
						</div>						
												<div class="price">
														<span class="price-new">$56.00</span> 
							<span class="price-old">$70.00</span>
																				</div>
											</div>
					<div class="cart-button">
						<button class="btn-primary" type="button"  onclick="ajaxAdd($(this),49);" > <i class="fl-outicons-shopping-cart13"></i> <span>Add to Cart</span> </button>
						<div class="clear"></div>
						<button class="product-btn" type="button" onclick="wishlist.add('49');"> <i class="material-icons-favorite_border"></i> <span>Add to Wishlist</span></button>
						<button class="product-btn" type="button" onclick="compare.add('49');"> <i class="material-icons-shuffle"></i> <span>Add to Compare</span></button>
					</div>
				</div>
														<div class="product-thumb transition options">
										<!-- Product options -->
					<div class="product-option-wrap">
						<div class="product-options form-horizontal">
							<div class="options">
								<a class="ajax-overlay_close" href='#'></a>
								<h3>Available Options</h3>
								<div class="form-group hidden">
									<div class="col-sm-8">
										<input type="text" name="product_id" value="43" class="form-control"/>
									</div>
								</div>
																								<div class="form-group required">
									<label class="control-label col-sm-12" for="input-option2702006">
										Size									</label>
									<div class="col-sm-12">
										<select name="option[27]" id="input-option2702006" class="form-control">
											<option value="">--- Please Select ---</option>
																						<option value="61">
												Small												(+$20.00)&lrm;											</option>
																						<option value="62">
												Medium												(+$15.00)&lrm;											</option>
																						<option value="63">
												Large												(+$10.00)&lrm;											</option>
																					</select>
									</div>
								</div>
																																																																																																																<div class="form-group required">
									<label class="control-label col-sm-12">
										Color									</label>
									<div class="col-sm-12">
										<div id="input-option2802006">
																						<div class="radio">
												<label for="option[286402006]">
													<input type="radio" hidden name="option[28]" id="option[286402006]" value="64"/>
													red													(+$10.00)&lrm;												</label>
											</div>
																					</div>
									</div>
								</div>
																																																																																								<button class="btn-primary" type="button" onclick="cart.addPopup($(this),'43');">
									<i class="fl-outicons-shopping-cart13"></i>  <span>Add to Cart</span>
								</button>
							</div>
						</div>
					</div>
										<div class="quick_info">
						<div id="quickview_20060" class="quickview-style">
							<div class="left col-sm-5">
								<div class="quickview_image image">
									<a href="https://livedemo00.template-help.com/opencart_59086/index.php?route=product/product&amp;product_id=43">
										<img width="310" height="310" alt="Anti-Aging Daily Moisturizer" title="Anti-Aging Daily Moisturizer" class="img-responsive" src="image/cache/catalog/products/product_37-310x310.png"/>
									</a>
								</div>
							</div>
							<div class="right col-sm-7">
								<h2>Anti-Aging Daily Moisturizer</h2>								
																<div class="rating">
																											<span class="fa-stack"> <i class="fa fa-star active fa-stack-1x"></i> </span>
																																				<span class="fa-stack"> <i class="fa fa-star active fa-stack-1x"></i> </span>
																																				<span class="fa-stack"> <i class="fa fa-star active fa-stack-1x"></i> </span>
																																				<span class="fa-stack"> <i class="fa fa-star active fa-stack-1x"></i> </span>
																																				<span class="fa-stack"> <i class="fa fa-star fa-stack-1x"></i> </span>
																										</div>
																<div class="inf">
																		<p class="quickview_manufacture manufacture manufacture">
										Brand:										<a href="https://livedemo00.template-help.com/opencart_59086/index.php?route=product/manufacturer/info&amp;manufacturer_id=27">Nivea</a>
									</p>
																											<p class="product_model model">
										Model: Premium									</p>
																		
								</div>								
																	<div class="price">
																				<span class="price-new">$64.00</span>
																													</div>
																<button class="btn-primary" data-toggle="tooltip" title="Add to Cart" type="button" onclick="cart.add('43');"> <i class="fl-outicons-shopping-cart13"></i> <span>Add to Cart</span> </button>
								<ul class="product-buttons">
									<li>
										<button class="product-btn" type="button" data-toggle="tooltip" title="Add to Wishlist" onclick="wishlist.add('43');">
											<i class="material-icons-favorite_border"></i> <span>Add to Wishlist</span>
										</button>
									</li>
									<li>
										<button class="product-btn" type="button" data-toggle="tooltip" title="Add to Compare" onclick="compare.add('43');">
											<i class="material-icons-shuffle"></i> <span>Add to Compare</span>
										</button>
									</li>
								</ul>
								<div class="clear"></div>
							</div>
						</div>
					</div>
															<div class="sale">
						<span>Sale</span>
					</div>
																									<div class="image">
						<a class="lazy" style="padding-bottom: 100%" href="https://livedemo00.template-help.com/opencart_59086/index.php?route=product/product&amp;product_id=43">
							<img width="310" height="310" alt="Anti-Aging Daily Moisturizer" title="Anti-Aging Daily Moisturizer" class="img-responsive" data-src="image/cache/catalog/products/product_37-310x310.png" src="#"/>
						</a>
						<a class="quickview" data-rel="details" href="#quickview_20060">
							Quick View						</a>
													<div class="rating">
																								<span class="fa-stack"> <i class="fa fa-star active fa-stack-1x"></i> </span>
																																<span class="fa-stack"> <i class="fa fa-star active fa-stack-1x"></i> </span>
																																<span class="fa-stack"> <i class="fa fa-star active fa-stack-1x"></i> </span>
																																<span class="fa-stack"> <i class="fa fa-star active fa-stack-1x"></i> </span>
																																<span class="fa-stack"> <i class="fa fa-star fa-stack-1x"></i> </span>
																							</div>
											</div>
					<div class="caption">						
						<div class="name">
							<a href="https://livedemo00.template-help.com/opencart_59086/index.php?route=product/product&amp;product_id=43">Anti-Aging Daily Moisturizer</a>
						</div>
						<div class="category-name">
							Face 
						</div>						
												<div class="price">
														<span class="price-new">$64.00</span> 
							<span class="price-old">$80.00</span>
																				</div>
											</div>
					<div class="cart-button">
						<button class="btn-primary" type="button"  onclick="ajaxAdd($(this),43);" > <i class="fl-outicons-shopping-cart13"></i> <span>Add to Cart</span> </button>
						<div class="clear"></div>
						<button class="product-btn" type="button" onclick="wishlist.add('43');"> <i class="material-icons-favorite_border"></i> <span>Add to Wishlist</span></button>
						<button class="product-btn" type="button" onclick="compare.add('43');"> <i class="material-icons-shuffle"></i> <span>Add to Compare</span></button>
					</div>
				</div>
									</div>
	</div>
</div>
<div class="box blog_articles">
	<div class="box-heading">
		<h3>Latest from our blog</h3>
		<h5>News and useful tips</h5>
	</div>
	<div class="box-content">
		<div class="row">
						<div class="col-sm-4">
				
								
								
									<figure class="article-image">
						<img width="225" height="225" src="image/cache/catalog/blog/post-10-225x225.jpg" alt="Going Green Is So Much Simpler Than Most People Think"/>
					</figure>
								
				<h3 class="article-title">
					<a href="https://livedemo00.template-help.com/opencart_59086/index.php?route=simple_blog/article/view&amp;simple_blog_article_id=10">Going Green Is So Much Simpler Than Most People Think</a>
				</h3>
				<div>
								<div class="article-description">An increasing number of people now want to do their part to save the planet due to the worsening problems caused by
   global warming. Unfortunately, numerous people ass...</div>
												</div>
			</div>
						<div class="col-sm-4">
				
								
								
									<figure class="article-image">
						<img width="225" height="225" src="image/cache/catalog/blog/post-9-225x225.jpg" alt="Feeling Stressed? Try the One Joke per Day Therapy"/>
					</figure>
								
				<h3 class="article-title">
					<a href="https://livedemo00.template-help.com/opencart_59086/index.php?route=simple_blog/article/view&amp;simple_blog_article_id=9">Feeling Stressed? Try the One Joke per Day Therapy</a>
				</h3>
				<div>
								<div class="article-description">There are so many elements today in our lives that make things more and more hectic by the day, and there hardly
       seems to be any respite from the constantly mount...</div>
												</div>
			</div>
						<div class="col-sm-4">
				
								
									<div class="r_frame article-video"> <iframe src="//www.youtube.com/embed/92BSaJhVoWg?rel=0&amp;controls=0&amp;showinfo=0&amp;wmode=transparent" allowfullscreen="" height="433" width="770" frameborder="0"></iframe> </div>
								
									<figure class="article-image">
						<img width="225" height="225" src="image/cache/catalog/blog/post-8-225x225.jpg" alt="5 Most Common Mistakes New Managers Make"/>
					</figure>
								
				<h3 class="article-title">
					<a href="https://livedemo00.template-help.com/opencart_59086/index.php?route=simple_blog/article/view&amp;simple_blog_article_id=8">5 Most Common Mistakes New Managers Make</a>
				</h3>
				<div>
								<div class="article-description">
Learn which five most common mistakes a new manager is likely to make, and how to avoid them.

    Mistake No.1 - Who's the Boss?


    Some of your subordinates w...</div>
												</div>
			</div>
					</div>
	</div>
</div>
</div>
			</div>
</div>
<footer>
	<div class="container">
		<div class="row">
			<div class="col-xs-6 col-sm-4 col-md-3 col-lg-3">
				<h4>Information</h4>
				<ul class="list-unstyled">
											<li><a href="https://livedemo00.template-help.com/opencart_59086/index.php?route=information/information&amp;information_id=4">About</a></li>
											<li><a href="https://livedemo00.template-help.com/opencart_59086/index.php?route=information/information&amp;information_id=6">Delivery Information</a></li>
											<li><a href="https://livedemo00.template-help.com/opencart_59086/index.php?route=information/information&amp;information_id=3">Privacy Policy</a></li>
											<li><a href="https://livedemo00.template-help.com/opencart_59086/index.php?route=information/information&amp;information_id=5">Terms &amp; Conditions</a></li>
											<li><a href="https://livedemo00.template-help.com/opencart_59086/index.php?route=information/contact">Contact Us</a></li>
						<li><a href="https://livedemo00.template-help.com/opencart_59086/index.php?route=information/sitemap">Site Map</a></li>					
				</ul>
			</div>
			<div class="col-xs-6 col-sm-4 col-md-3 col-lg-3">
				<h4>Extras</h4>
				<ul class="list-unstyled">
					<li><a href="https://livedemo00.template-help.com/opencart_59086/index.php?route=product/manufacturer">Brands</a></li>
					<li><a href="https://livedemo00.template-help.com/opencart_59086/index.php?route=account/voucher">Gift Certificates</a></li>
					<li><a href="https://livedemo00.template-help.com/opencart_59086/index.php?route=affiliate/account">Affiliates</a></li>
					<li><a href="https://livedemo00.template-help.com/opencart_59086/index.php?route=product/special">Specials</a></li>
											<li><a href="https://livedemo00.template-help.com/opencart_59086/index.php?route=simple_blog/article">Blog</a></li>
									</ul>
			</div>
			<div class="col-xs-6 col-sm-4 col-md-3 col-lg-3">
				<h4>My Account</h4>
				<ul class="list-unstyled">
					<li><a href="https://livedemo00.template-help.com/opencart_59086/index.php?route=account/account">My Account</a></li>
					<li><a href="https://livedemo00.template-help.com/opencart_59086/index.php?route=account/order">Order History</a></li>
					<li><a href="https://livedemo00.template-help.com/opencart_59086/index.php?route=account/wishlist">Wish List</a></li>
					<li><a href="https://livedemo00.template-help.com/opencart_59086/index.php?route=account/newsletter">Newsletter</a></li>
				</ul>
			</div>			
			<div class="col-xs-6 col-sm-12 col-md-3 col-lg-3">
									<div class="footer_modules"><!-- begin olark code -->
<script data-cfasync="false" type='text/javascript'>/*<![CDATA[*/window.olark||(function(c){var f=window,d=document,l=f.location.protocol=="https:"?"https:":"http:",z=c.name,r="load";var nt=function(){
f[z]=function(){
(a.s=a.s||[]).push(arguments)};var a=f[z]._={
},q=c.methods.length;while(q--){(function(n){f[z][n]=function(){
f[z]("call",n,arguments)}})(c.methods[q])}a.l=c.loader;a.i=nt;a.p={
0:+new Date};a.P=function(u){
a.p[u]=new Date-a.p[0]};function s(){
a.P(r);f[z](r)}f.addEventListener?f.addEventListener(r,s,false):f.attachEvent("on"+r,s);var ld=function(){function p(hd){
hd="head";return["<",hd,"></",hd,"><",i,' onl' + 'oad="var d=',g,";d.getElementsByTagName('head')[0].",j,"(d.",h,"('script')).",k,"='",l,"//",a.l,"'",'"',"></",i,">"].join("")}var i="body",m=d[i];if(!m){
return setTimeout(ld,100)}a.P(1);var j="appendChild",h="createElement",k="src",n=d[h]("div"),v=n[j](d[h](z)),b=d[h]("iframe"),g="document",e="domain",o;n.style.display="none";m.insertBefore(n,m.firstChild).id=z;b.frameBorder="0";b.id=z+"-loader";if(/MSIE[ ]+6/.test(navigator.userAgent)){
b.src="javascript:false"}b.allowTransparency="true";v[j](b);try{
b.contentWindow[g].open()}catch(w){
c[e]=d[e];o="javascript:var d="+g+".open();d.domain='"+d.domain+"';";b[k]=o+"void(0);"}try{
var t=b.contentWindow[g];t.write(p());t.close()}catch(x){
b[k]=o+'d.write("'+p().replace(/"/g,String.fromCharCode(92)+'"')+'");d.close();'}a.P(2)};ld()};nt()})({
loader: "static.olark.com/jsclient/loader0.js",name:"olark",methods:["configure","extend","declare","identify"]});
/* custom configuration goes here (www.olark.com/documentation) */
olark.identify('7830-582-10-3714');/*]]>*/</script><noscript><a href="https://www.olark.com/site/7830-582-10-3714/contact" title="Contact us" target="_blank">Questions? Feedback?</a> powered by <a href="https://www.olark.com?welcome" title="Olark live chat software">Olark live chat software</a></noscript>
<!-- end olark code --><h4>Social News</h4>
<p>The latest news delivered across all social networks!</p>
<ul class="social-list list-unstyled">
		<li><a class="fa fa-facebook" href="#" data-toggle="tooltip" title="Facebook"></a></li>
		<li><a class="fa fa-twitter" href="#" data-toggle="tooltip" title="Twitter"></a></li>
		<li><a class="fa fa-google-plus" href="#" data-toggle="tooltip" title="Google+"></a></li>
		<li><a class="fa fa-pinterest-p" href="#" data-toggle="tooltip" title="Pinterest"></a></li>
		<li><a class="fa fa-instagram" href="#" data-toggle="tooltip" title="Instagram"></a></li>
	</ul><div id="tm-newsletter" class="box newsletter">	
	<div class="box-content clearfix">
		<div>
			<h4>Get the latest news delivered daily!</h4>
			<p class="newsletter-description">Give us your email and you will be daily updated with the latest events, in detail!</p>		</div>
		<form method="post" enctype="multipart/form-data" id="tm-newsletter-form">
			<div class="tm-login-form">
				<label class="control-label" for="input-tm-newsletter-email"></label>
				<input type="text" name="tm_newsletter_email" value="" placeholder="Enter your e-mail"
				id="input-tm-newsletter-email" class="form-control"/>
				<button type="submit" id="tm-newsletter-button" class="fa fa-envelope-o newsletter-button"></button>
			</div>
			<span id="tm-newsletter_error" class="newsletter-error"></span>
			<span id="tm-newsletter_success" class="newsletter-success"></span>			
		</form>
	</div>
</div>
<script>
	$(document).ready(function () {
		$('#tm-newsletter').submit(function (e) {
			e.preventDefault();
			var email = $("#input-tm-newsletter-email").val();
			var emailRegex = /^[A-Z0-9._%+-]+@[A-Z0-9.-]+.[A-Z]{2,4}$/igm;
			if (emailRegex.test(email)) {
				var dataString = 'tm_newsletter_email=' + email;
				$.ajax({
					type: "POST",
					url: "index.php?route=module/tm_newsletter/addNewsletter",
					data: dataString,
					cache: false,
					success: function (result) {
						if (!result){
							$('#tm-newsletter_error').html('');
							$('#tm-newsletter_success').stop(true, true).html('You have successfully subscribed').fadeIn(300).delay(4000).fadeOut(300);
						}else{
							$('#tm-newsletter_success').html('');
							$('#tm-newsletter_error').stop(true, true).html(result).fadeIn(300).delay(4000).fadeOut(300);
						}
					}
				});
			} else {
				$('#tm-newsletter_success').html('');
				$('#tm-newsletter_error').stop(true, true).html('Please enter a valid e-mail!').fadeIn(300).delay(4000).fadeOut(300);
			}
		});
	});
</script></div>
							</div>
		</div>				
	</div>
	<div class="copyright">
		<div class="container">
			Powered By <a href="https://www.opencart.com">OpenCart</a><br /> Cosmetorix &copy; 2019<!-- [[%FOOTER_LINK]] -->
		</div>
	</div>
</footer>
<div class="ajax-overlay"></div>
</div>
<script src="catalog/view/theme/theme694/js/device.min.js" type="text/javascript"></script>
<script src="catalog/view/theme/theme694/js/livesearch.min.js" type="text/javascript"></script>
<script src="catalog/view/theme/theme694/js/common.js" type="text/javascript"></script>
<script src="catalog/view/theme/theme694/js/script.js" type="text/javascript"></script>
<!-- code by xena -->
<script type="text/javascript">
 var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-7078796-5']);
  _gaq.push(['_trackPageview']);
  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'https://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();</script>
</body><!-- Google Tag Manager --><noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-P9FT69"height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript><script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start': new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src='//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);})(window,document,'script','dataLayer','GTM-P9FT69');</script><!-- End Google Tag Manager --></html>